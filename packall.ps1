$NEXT_VERSION=$(dotnet gitversion -output json -showvariable NuGetVersionV2)
Write-Output $NEXT_VERSION
Get-Content 'src\*.sln' |
  Select-String 'Project\(' |
    ForEach-Object {
      $projectParts = $_ -Split '[,=]' | ForEach-Object { $_.Trim('[ "{}]') };
      Write-Output "src\$($projectParts[2])"
      dotnet version -f "src\$($projectParts[2])" -s $NEXT_VERSION
    }
dotnet pack src -p:IncludeSymbols=true -p:SymbolPackageFormat=snupkg --configuration Debug

Get-ChildItem -Path "src\*" -Include *.nupkg,*.snupkg -Recurse | Copy-Item -Destination \\d-sp10\C$\coscine\LocalNugetFeeds\issue-2072-wormresourcetype