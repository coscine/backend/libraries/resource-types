﻿using Coscine.ResourceTypes.Base.Models;

namespace Coscine.ResourceTypes.Base;

public abstract class BaseResourceType
{
    public ResourceTypeConfiguration ResourceTypeConfiguration { get; set; }

    protected BaseResourceType(ResourceTypeConfiguration resourceTypeConfiguration)
    {
        ResourceTypeConfiguration = resourceTypeConfiguration;
    }

    public abstract Task<ResourceTypeInformation> GetResourceTypeInformation();

    public abstract Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string>? options = null);

    public abstract Task<ResourceEntry?> GetEntry(string id, string key, Dictionary<string, string>? options = null);

    public abstract Task StoreEntry(string id, string key, Stream body, Dictionary<string, string>? options = null);

    public abstract Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string>? options = null);

    public abstract Task DeleteEntry(string id, string key, Dictionary<string, string>? options = null);

    public abstract Task<Stream?> LoadEntry(string id, string key, Dictionary<string, string>? options = null);

    public abstract Task AddPrefix(string id, string prefix, Dictionary<string, string>? options = null);

    public abstract Task CreateResource(string id, long? quota = null, Dictionary<string, string>? options = null);

    public abstract Task UpdateResource(string id, Dictionary<string, string>? options = null);

    public abstract Task<bool> IsResourceCreated(string id, Dictionary<string, string>? options = null);

    public abstract Task DeleteResource(string id, Dictionary<string, string>? options = null);

    public abstract Task SetResourceQuota(string id, int quota, Dictionary<string, string>? options = null);

    public abstract Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string>? options = null);

    public abstract Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string>? options = null);

    public abstract Task<Uri> GetPresignedUrl(string id, string key, CoscineHttpVerb httpVerb, Dictionary<string, string>? options = null);

    public abstract Task SetResourceReadonly(string id, bool status, Dictionary<string, string>? options = null);
}