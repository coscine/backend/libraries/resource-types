﻿namespace Coscine.ResourceTypes.Base.Helpers;

public static class EmbeddedResourceLoader
{
    public static string ReadResource<TBaseClass>(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            throw new ArgumentException($"'{nameof(name)}' cannot be null or whitespace.", nameof(name));
        }

        // Determine path
        var assembly = typeof(TBaseClass).Assembly;
        string resourcePath = name;

        // Format: "{Namespace}.{Folder}.{filename}.{Extension}"
        if (!name.StartsWith(nameof(TBaseClass)))
        {
            resourcePath = assembly.GetManifestResourceNames().Single(x => x.EndsWith(name));
        }

        using var stream = assembly.GetManifestResourceStream(resourcePath);

        if (stream is null)
        {
            throw new KeyNotFoundException($"The resource '{resourcePath}' cannot be found.");
        }

        using var reader = new StreamReader(stream);
        return reader.ReadToEnd();
    }
}