﻿namespace Coscine.ResourceTypes.Base.Models;

/// <summary>
/// Object containing the relevant information about the resource type for the metadata VUE component.
/// </summary>
[Serializable]
public class MetadataView
{
    /// <summary>
    /// A dataUrl can be provided.
    /// </summary>
    public bool EditableDataUrl { get; set; }

    /// <summary>
    /// A key can be provided.
    /// </summary>
    public bool EditableKey { get; set; }

    /// <summary>
    /// Standard constructor.
    /// </summary>
    public MetadataView()
    {
    }

    /// <summary>
    /// Parameterized constructor.
    /// </summary>
    /// <param name="editableDataUrl">A dataUrl can be provided.</param>
    /// <param name="editableKey">A key can be provided.</param>
    public MetadataView(bool editableDataUrl, bool editableKey)
    {
        EditableDataUrl = editableDataUrl;
        EditableKey = editableKey;
    }
}