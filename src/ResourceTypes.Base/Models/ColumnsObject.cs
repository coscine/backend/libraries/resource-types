﻿namespace Coscine.ResourceTypes.Base.Models;

/// <summary>
/// Object containing the relevant information about the resource type for the columns within entries view VUE component.
/// </summary>
[Serializable]
public class ColumnsObject
{
    /// <summary>
    /// List of columns that should always be displayed in the entries view.
    /// </summary>
    public List<string> Always { get; set; } = new List<string>() { "name", "size" };

    /// <summary>
    /// Standard constructor.
    /// </summary>
    public ColumnsObject()
    {
    }

    /// <summary>
    /// Parameterized constructor.
    /// </summary>
    /// <param name="always">List of columns that should always be displayed in the entries view.</param>
    public ColumnsObject(List<string> always)
    {
        Always = always ?? throw new ArgumentNullException(nameof(always));
    }
}