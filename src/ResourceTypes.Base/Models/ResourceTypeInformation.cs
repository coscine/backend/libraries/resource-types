﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Coscine.ResourceTypes.Base.Models;

/// <summary>
/// Object containing all relevant information about the resource type for the fronted.
/// </summary>
[Serializable]
public class ResourceTypeInformation
{
    /// <summary>
    /// The resource type is enabled.
    /// </summary>
    public bool IsEnabled => Status == ResourceTypeStatus.Active;

    /// <summary>
    /// The resource type is enabled.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public ResourceTypeStatus Status { get; set; }

    /// <summary>
    /// Supports creation
    /// </summary>
    public bool CanCreate { get; set; }

    /// <summary>
    /// Supports reading
    /// </summary>
    public bool CanRead { get; set; }
	
	/// <summary>
    /// Supports reading
    /// </summary>
    public bool CanSetResourceReadonly { get; set; }

    /// <summary>
    /// Supports updating
    /// </summary>
    public bool CanUpdate { get; set; }

    /// <summary>
    /// Supports updating (not an Object)
    /// </summary>
    public bool CanUpdateResource { get; set; }

    /// <summary>
    /// Supports deletion
    /// </summary>
    public bool CanDelete { get; set; }

    /// <summary>
    /// Supports deletion (not an Object)
    /// </summary>
    public bool CanDeleteResource { get; set; }

    /// <summary>
    /// Supports listing
    /// </summary>
    public bool CanList { get; set; }

    /// <summary>
    /// Supports listing
    /// </summary>
    public bool CanCreateLinks { get; set; }

    /// <summary>
    /// The resource type is archived.
    /// </summary>
    public bool IsArchived { get; set; }

    /// <summary>
    /// Value to see, if the resource has a quota.
    /// </summary>
    public bool IsQuotaAvailable { get; set; }

    /// <summary>
    /// Value to see, if the resource quota can be changed.
    /// </summary>
    public bool IsQuotaAdjustable { get; set; }

    /// <summary>
    /// Display name of the resource type.
    /// </summary>
    [Obsolete($"Deleting this field in future versions in favor of {nameof(GeneralType)} and {nameof(SpecificType)}.")]
    public string? DisplayName { get; set; }

    /// <summary>
    /// General resource type.
    /// </summary>
    public string? GeneralType { get; set; }

    /// <summary>
    /// Specific resource type.
    /// </summary>
    public string? SpecificType { get; set; }

    /// <summary>
    /// Guid of the resource type.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Object containing the relevant information about the resource type for the resource create page.
    /// </summary>
    public ResourceCreateObject ResourceCreate { get; set; } = new ResourceCreateObject();

    /// <summary>
    /// Object containing the relevant information about the resource type for the resource content page.
    /// </summary>
    public ResourceContentObject ResourceContent { get; set; } = new ResourceContentObject();
}