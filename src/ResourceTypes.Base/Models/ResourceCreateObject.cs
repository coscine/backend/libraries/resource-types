﻿namespace Coscine.ResourceTypes.Base.Models;

/// <summary>
/// Object containing the relevant information about the resource type for the resource create page.
/// </summary>
[Serializable]
public class ResourceCreateObject
{
    /// <summary>
    /// List of Lists containing all the resource type specific components for the steps in the resource creation page.
    /// </summary>
    public List<List<string>> Components { get; set; } = null!;
}