﻿namespace Coscine.ResourceTypes.Base.Models;

public class Name
{
    public string? De { get; set; }
    public string? En { get; set; }
}