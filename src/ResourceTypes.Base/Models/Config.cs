﻿namespace Coscine.ResourceTypes.Base.Models;

public class Config
{
    public string? RdsKey { get; set; }
    public string? Rdss3Key { get; set; }
    public string? UserKey { get; set; }
}