﻿namespace Coscine.ResourceTypes.Base.Models;

/// <summary>
/// Object containing the relevant information about the resource type for the entries view VUE component.
/// </summary>
[Serializable]
public class EntriesView
{
    /// <summary>
    /// Object containing the relevant information about the resource type for the columns within entries view VUE component.
    /// </summary>
    public ColumnsObject Columns { get; set; } = new ColumnsObject();

    /// <summary>
    /// Standard constructor.
    /// </summary>
    public EntriesView()
    {
    }

    /// <summary>
    /// Parameterized constructor.
    /// </summary>
    /// <param name="columns">Object containing the relevant information about the resource type for the columns within entries view VUE component.</param>
    public EntriesView(ColumnsObject columns)
    {
        Columns = columns ?? throw new ArgumentNullException(nameof(columns));
    }
}