﻿namespace Coscine.ResourceTypes.Base.Models;

public class SpecificType
{
    public string? Type { get; set; }
    public string? SpecificTypeName { get; set; }
    public Config? Config { get; set; }
    public Name? Name { get; set; }
    public ResourceTypeStatus? Status { get; set; }
    public List<string>? SupportedOrganizations { get; set; }
}