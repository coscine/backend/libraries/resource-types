﻿namespace Coscine.ResourceTypes.Base.Models;

/// <summary>
/// Object containing the relevant information about the resource type for the resource content page.
/// </summary>
[Serializable]
public class ResourceContentObject
{
    /// <summary>
    /// Resource is read only.
    /// </summary>
    public bool ReadOnly { get; set; }

    /// <summary>
    /// Object containing the relevant information about the resource type for the metadata VUE component.
    /// </summary>
    public MetadataView MetadataView { get; set; } = new MetadataView();

    /// <summary>
    /// Object containing the relevant information about the resource type for the entries view VUE component.
    /// </summary>
    public EntriesView EntriesView { get; set; } = new EntriesView();

    /// <summary>
    /// Standard constructor.
    /// </summary>
    public ResourceContentObject()
    {
    }

    /// <summary>
    /// Parameterized constructor.
    /// </summary>
    /// <param name="resourceIsReadonly">Resource is read only.</param>
    /// <param name="metadataViewObj">Object containing the relevant information about the resource type for the metadata VUE component.</param>
    /// <param name="entriesViewObj">Object containing the relevant information about the resource type for the entries view VUE component.</param>
    public ResourceContentObject(bool resourceIsReadonly, MetadataView metadataViewObj, EntriesView entriesViewObj)
    {
        ReadOnly = resourceIsReadonly;
        MetadataView = metadataViewObj ?? throw new ArgumentNullException(nameof(metadataViewObj));
        EntriesView = entriesViewObj ?? throw new ArgumentNullException(nameof(entriesViewObj));
    }
}