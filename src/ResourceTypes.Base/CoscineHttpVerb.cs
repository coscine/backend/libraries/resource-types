﻿namespace Coscine.ResourceTypes.Base;

//
// Summary:
//     An enumeration of supported HTTP verbs
public enum CoscineHttpVerb
{
    //
    // Summary:
    //     The GET HTTP verb.
    GET,

    //
    // Summary:
    //     The HEAD HTTP verb.
    HEAD,

    //
    // Summary:
    //     The PUT HTTP verb.
    PUT,

    //
    // Summary:
    //     The DELETE HTTP verb.
    DELETE
}