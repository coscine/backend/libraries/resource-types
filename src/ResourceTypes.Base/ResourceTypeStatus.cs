﻿using System.Runtime.Serialization;

namespace Coscine.ResourceTypes.Base;

public enum ResourceTypeStatus
{
    [EnumMember(Value = "hidden")]
    Hidden,

    [EnumMember(Value = "active")]
    Active
}