﻿using Coscine.ResourceTypes.Base.Models;

namespace Coscine.ResourceTypes.Base;

public class ResourceTypeConfiguration
{
    public SpecificType? SpecificType { get; set; }
}