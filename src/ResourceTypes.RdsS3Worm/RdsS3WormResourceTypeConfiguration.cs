﻿using Coscine.ECSManager;
using Coscine.ResourceTypes.Base;

namespace Coscine.ResourceTypes.RdsS3Worm;

public class RdsS3WormResourceTypeConfiguration : ResourceTypeConfiguration
{
    public EcsManagerConfiguration? RdsS3EcsManagerConfiguration { get; set; }
    public EcsManagerConfiguration? UserEcsManagerConfiguration { get; set; }
    public IHttpClientFactory? HttpClientFactory { get; set; }
    public string? AccessKey { get; set; }
    public string? SecretKey { get; set; }
    public string? AccessKeyRead { get; set; }
    public string? SecretKeyRead { get; set; }
    public string? AccessKeyWrite { get; set; }
    public string? SecretKeyWrite { get; set; }
    public string? Endpoint { get; set; }
}