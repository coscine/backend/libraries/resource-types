﻿using Amazon.S3;
using Coscine.ResourceTypes.Base;

namespace ResourceTypes.Rds.Helpers;

internal static class Helper
{
    public static HttpVerb GetVerb(CoscineHttpVerb httpMethod)
    {
        return httpMethod switch
        {
            CoscineHttpVerb.GET => HttpVerb.GET,
            CoscineHttpVerb.PUT => HttpVerb.PUT,
            CoscineHttpVerb.HEAD => HttpVerb.HEAD,
            CoscineHttpVerb.DELETE => HttpVerb.DELETE,
            _ => throw new ArgumentException($@"""{httpMethod}"" is not a valid option. Must be either CoscineHttpVerb.GET, CoscineHttpVerb.PUT, CoscineHttpVerb.HEAD or CoscineHttpVerb.DELETE.", nameof(httpMethod)),
        };
    }
}