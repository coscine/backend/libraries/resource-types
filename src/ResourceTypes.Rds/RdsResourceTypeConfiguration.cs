﻿using Coscine.ECSManager;
using Coscine.ResourceTypes.Base;

namespace Coscine.ResourceTypes.Rds;

public class RdsResourceTypeConfiguration : ResourceTypeConfiguration
{
    public EcsManagerConfiguration? EcsManagerConfiguration { get; set; }
    public IHttpClientFactory? HttpClientFactory { get; set; }
    public string? AccessKey { get; set; }
    public string? SecretKey { get; set; }
    public string? Endpoint { get; set; }
}