﻿using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.ResourceTypes.Base;

namespace Coscine.ResourceTypes;

public static class Helpers
{
    private const float _oneGiB = 1024 * 1024 * 1024; // 1 GiB = 1024³ Bytes

    /// <summary>
    /// Returns the total reserved quota for a resource type in project in GibiBYTE [GiB].
    /// </summary>
    /// <param name="resourceType">Resource type.</param>
    /// <param name="projectId">Id of the project.</param>
    /// <returns>Total reserved quota for a resource type in project in GibiBYTE [GiB].</returns>
    public static QuotaDimObject CalculateTotalReservedQuota(ResourceType resourceType, Guid projectId)
    {
        var _resourceModel = new ResourceModel();
        var resources = _resourceModel.GetAllWhere((resource) =>
                    (from projectResource in resource.ProjectResources
                     where projectResource.ProjectId == projectId
                     select projectResource).Any() &&
                     resource.TypeId == resourceType.Id);

        var totalReserved = resources.Sum(resource =>
        {
            // Linked has no quota.
            var baseResourceType = ResourceTypeFactory.Instance.GetResourceType(resource);
            if (baseResourceType.GetResourceTypeInformation().Result.IsQuotaAvailable)
            {
                try
                {
                    return baseResourceType.GetResourceQuotaAvailable(resource.Id.ToString(), _resourceModel.GetResourceTypeOptions(resource.Id)).Result;
                } 
                catch (Exception)
                {
                    // Error in communicating with the resource
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        });

        return new QuotaDimObject
        {
            Value = totalReserved,
            Unit = QuotaUnit.GibiBYTE
        };
    }

    /// <summary>
    /// Returns the total used quota for a resource type in project in GibiBYTE [GiB].
    /// </summary>
    /// <param name="resourceType">Resource type.</param>
    /// <param name="projectId">Id of the project.</param>
    /// <returns>Total used quota for a resource type in project in GibiBYTE [GiB].</returns>
    public static QuotaDimObject CalculateTotalUsedQuota(ResourceType resourceType, Guid projectId)
    {
        var _resourceModel = new ResourceModel();
        var resources = _resourceModel.GetAllWhere((resource) =>
                    (from projectResource in resource.ProjectResources
                     where projectResource.ProjectId == projectId
                     select projectResource).Any() &&
                     resource.TypeId == resourceType.Id);

        var totalUsed = resources.Sum(resource =>
        {
            // Linked has no quota.
            var baseResourceType = ResourceTypeFactory.Instance.GetResourceType(resource);
            if (baseResourceType.GetResourceTypeInformation().Result.IsQuotaAvailable)
            {
                return baseResourceType.GetResourceQuotaUsed(resource.Id.ToString(), _resourceModel.GetResourceTypeOptions(resource.Id)).Result;
            }
            else
            {
                return 0;
            }
        });
        return new QuotaDimObject
        {
            Value = totalUsed,
            Unit = QuotaUnit.GibiBYTE
        };
    }

    /// <summary>
    /// Converts between different capacity units.
    /// </summary>
    /// <param name="input">Existing QuotaDimObject as input.</param>
    /// <param name="outputUnit">Desired QuotaUnit output.</param>
    /// <returns>A number in the desired output capacity unit as a float.</returns>
    public static float ConvertCapacityUnits(QuotaDimObject input, QuotaUnit outputUnit)
    {
        const int k = 1024;
        var sizes = new List<QuotaUnit>() {
          QuotaUnit.BYTE,
          QuotaUnit.KibiBYTE,
          QuotaUnit.MebiBYTE,
          QuotaUnit.GibiBYTE,
          QuotaUnit.TebiBYTE,
          QuotaUnit.PebiBYTE,
        };

        var input_exponent = sizes.FindIndex((v) => v == input.Unit);
        var output_exponent = sizes.FindIndex((v) => v == outputUnit);

        return (float)(input.Value * Math.Pow(k, input_exponent - output_exponent));
    }

    /// <summary>
    /// Returns the maximum quota available for a project.
    /// </summary>
    /// <param name="projectId">Id of the project.</param>
    /// <param name="resourceTypeId">Id of the resource type.</param>
    /// <returns>Maximum quota available for a project.</returns>
    public static QuotaDimObject GetMaximumQuota(Guid projectId, Guid resourceTypeId)
    {
        var _projectQuotaModel = new ProjectQuotaModel();
        var projectQuota = _projectQuotaModel.GetWhere(x => x.ProjectId == projectId && x.ResourceTypeId == resourceTypeId);
        return new QuotaDimObject
        {
            Value = projectQuota?.MaxQuota ?? 0,
            Unit = QuotaUnit.GibiBYTE
        };
    }

    /// <summary>
    /// Returns a ResourceObject with its Size property populated.
    /// </summary>
    /// <param name="resource">The current resource as a Resource object.</param>
    /// <returns>ResourceObject with its Size property populated.</returns>
    public static ResourceObject CreateResourceReturnObject(Resource resource)
    {
        var _resourceModel = new ResourceModel();
        var returnObject = _resourceModel.CreateReturnObjectFromDatabaseObject(resource);

        if (returnObject.ResourceTypeOption.ContainsKey("Size"))
        {
            var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);
            try
            {
                returnObject.ResourceTypeOption["Size"] = resourceTypeDefinition.GetResourceQuotaAvailable(resource.Id.ToString()).Result;
            } 
            catch (Exception)
            {
                // Error in communicating with the resource
                returnObject.ResourceTypeOption["Size"] = 0;
            }
        }

        return returnObject;
    }

    /// <summary>
    /// Returns a ResourceQuotaReturnObject.
    /// </summary>
    /// <param name="resource">The used resource.</param>
    /// <param name="resourceTypeDefinition">The used ResourceTypeDefinition.</param>
    /// <returns>A ResourceQuotaReturnObject.</returns>
    public static ResourceQuotaReturnObject CreateResourceQuotaReturnObject(Resource resource, BaseResourceType resourceTypeDefinition)
    {
        var usedBytes = resourceTypeDefinition.GetResourceQuotaUsed(resource.Id.ToString()).Result;
        var reservedGiB = resourceTypeDefinition.GetResourceQuotaAvailable(resource.Id.ToString()).Result;

        return new ResourceQuotaReturnObject
        {
            Id = resource.Id,
            Name = resource.DisplayName,
            Used = new QuotaDimObject()
            {
                Value = usedBytes,
                Unit = QuotaUnit.BYTE,
            },
            UsedPercentage = 100 * (usedBytes / (reservedGiB * _oneGiB)), // _oneGiB is float -> float division
            Reserved = new QuotaDimObject()
            {
                Value = reservedGiB,
                Unit = QuotaUnit.GibiBYTE, // 1 GiB = 1024³ Bytes
            },
        };
    }

    /// <summary>
    /// Returns a ProjectQuotaReturnObject.
    /// </summary>
    /// <param name="resourceType">Resource type.</param>
    /// <param name="projectId">Id of the project.</param>
    /// <returns>A ProjectQuotaReturnObject.</returns>
    public static ProjectQuotaReturnObject CreateProjectQuotaReturnObject(ResourceType resourceType, Guid projectId)
    {
        var _projectQuotaModel = new ProjectQuotaModel();
        var projectQuota = _projectQuotaModel.GetWhere((y) =>
                y.ProjectId == projectId &&
                y.ResourceTypeId == resourceType.Id);

        return new ProjectQuotaReturnObject
        {
            Id = resourceType.Id,
            Name = resourceType.SpecificType,
            TotalReserved = CalculateTotalReservedQuota(resourceType, projectId),
            Allocated = new QuotaDimObject()
            {
                Value = (projectQuota?.Quota) ?? 0,
                Unit = QuotaUnit.GibiBYTE, // 1 GiB = 1024³ Bytes
            },
            Maximum = new QuotaDimObject()
            {
                Value = (projectQuota?.MaxQuota) ?? 0,
                Unit = QuotaUnit.GibiBYTE, // 1 GiB = 1024³ Bytes
            }
        };
    }

    /// <summary>
    /// Returns a ProjectQuotaExtendedReturnObject.
    /// </summary>
    /// <param name="resourceType">Resource type.</param>
    /// <param name="projectId">Id of the project.</param>
    /// <returns>A ProjectQuotaReturnObject.</returns>
    public static ProjectQuotaExtendedReturnObject CreateProjectQuotaExtendedReturnObject(ResourceType resourceType, Guid projectId)
    {
        var _projectQuotaModel = new ProjectQuotaModel();
        var _resourceModel = new ResourceModel();

        var projectQuota = _projectQuotaModel.GetWhere((y) =>
                y.ProjectId == projectId &&
                y.ResourceTypeId == resourceType.Id);

        var resources = _resourceModel.GetAllWhere((resource) =>
            (from projectResource in resource.ProjectResources
                where projectResource.ProjectId == projectId
                select projectResource).Any() &&
                resource.TypeId == resourceType.Id);

        return new ProjectQuotaExtendedReturnObject
        {
            Id = resourceType.Id,
            Name = resourceType.SpecificType,
            TotalReserved = CalculateTotalReservedQuota(resourceType, projectId),
            Allocated = new QuotaDimObject()
            {
                Value = (projectQuota?.Quota) ?? 0,
                Unit = QuotaUnit.GibiBYTE, // 1 GiB = 1024³ Bytes
            },
            Maximum = new QuotaDimObject()
            {
                Value = (projectQuota?.MaxQuota) ?? 0,
                Unit = QuotaUnit.GibiBYTE, // 1 GiB = 1024³ Bytes
            },
            ResourcesQuota = resources.Select(resource =>
            {
                var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);
                return CreateResourceQuotaReturnObject(resource, resourceTypeDefinition);
            }).ToList()
        };
    }
}
