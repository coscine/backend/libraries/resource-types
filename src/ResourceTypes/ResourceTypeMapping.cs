﻿namespace Coscine.ResourceTypes
{
    public class ResourceTypeMapping
    {
        public Dictionary<string, string> SpecificTypes { get; } = null!;
        public Dictionary<string, string> Types { get; } = null!;
    }
}