﻿namespace Coscine.ResourceTypes
{
    internal sealed class DefaultHttpClientFactory : IHttpClientFactory
    {
        private static readonly Lazy<HttpClient> _httpClientLazy =
            new(() => new HttpClient());

        public HttpClient CreateClient(string name) => _httpClientLazy.Value;
    }
}