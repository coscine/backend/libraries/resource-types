﻿namespace Coscine.ResourceTypes.ResourceTypeConfigs
{
    public class GetRdsResourceTypeConfigOptions : GetResourceTypeConfigOptions
    {
        public string? Bucketname { get; set; }
    }
}