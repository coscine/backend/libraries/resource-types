﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.ECSManager;
using Coscine.ResourceTypes.Base;
using Coscine.ResourceTypes.Base.Helpers;
using Coscine.ResourceTypes.Base.Models;
using Coscine.ResourceTypes.GitLab;
using Coscine.ResourceTypes.Linked;
using Coscine.ResourceTypes.Rds;
using Coscine.ResourceTypes.RdsS3;
using Coscine.ResourceTypes.RdsS3Worm;
using Coscine.ResourceTypes.ResourceTypeConfigs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coscine.ResourceTypes;

public sealed class ResourceTypeFactory
{
    private static readonly Lazy<ResourceTypeFactory> lazy =
        new(() => new ResourceTypeFactory());

    public static ResourceTypeFactory Instance
    { get { return lazy.Value; } }

    public static IHttpClientFactory HttpClientFactory { get; set; } = new DefaultHttpClientFactory();

    private readonly Dictionary<string, SpecificType> _specificTypes;

    private ResourceTypeFactory()
    {
        _specificTypes = LoadSpecificTypes();
    }

    private static Dictionary<string, SpecificType> LoadSpecificTypes()
    {
        var jsonString = EmbeddedResourceLoader.ReadResource<ResourceTypeFactory>("SpecificTypesConfig.json");
        var jsonObj = JsonConvert.DeserializeObject<JObject>(jsonString)
                                      ?? throw new Exception("SpecificTypesConfig.json could not be parsed.");

        var types = new Dictionary<string, SpecificType>();

        foreach (var kv in jsonObj)
        {
            if (kv.Value?.HasValues == true)
            {
                var st = kv.Value.ToObject<SpecificType>();
                if (st is not null)
                {
                    st.SpecificTypeName = kv.Key;
                    types.Add(kv.Key, st);
                }
            }
        }

        return types;
    }

    public static BaseResourceType GetResourceType(string type, ResourceTypeConfiguration resourceTypeOptions)
    {
        if (string.IsNullOrWhiteSpace(type))
        {
            throw new ArgumentException($"'{nameof(type)}' cannot be null or whitespace.", nameof(type));
        }

        if (resourceTypeOptions is null)
        {
            throw new ArgumentNullException(nameof(resourceTypeOptions));
        }

        switch (type.ToLower())
        {
            case "gitlab":
                if (!resourceTypeOptions.GetType().IsAssignableFrom(typeof(GitLabResourceTypeConfiguration)))
                {
                    throw new ArgumentException("No gitlab resource type options provided.", nameof(resourceTypeOptions));
                }

                return new GitLabResourceType((GitLabResourceTypeConfiguration)resourceTypeOptions);

            case "linked":
                if (!resourceTypeOptions.GetType().IsAssignableFrom(typeof(LinkedResourceTypeConfiguration)))
                {
                    throw new ArgumentException("No linked resource type options provided.", nameof(resourceTypeOptions));
                }

                return new Linked.LinkedResourceType((LinkedResourceTypeConfiguration)resourceTypeOptions);

            case "rds":
                if (!resourceTypeOptions.GetType().IsAssignableFrom(typeof(RdsResourceTypeConfiguration)))
                {
                    throw new ArgumentException("No rds resource type options provided.", nameof(resourceTypeOptions));
                }

                return new RdsResourceType((RdsResourceTypeConfiguration)resourceTypeOptions);

            case "rdss3":
                if (!resourceTypeOptions.GetType().IsAssignableFrom(typeof(RdsS3ResourceTypeConfiguration)))
                {
                    throw new ArgumentException("No rdss3 resource type options provided.", nameof(resourceTypeOptions));
                }

                return new RdsS3ResourceType((RdsS3ResourceTypeConfiguration)resourceTypeOptions);

            case "rdss3worm":
                if (!resourceTypeOptions.GetType().IsAssignableFrom(typeof(RdsS3WormResourceTypeConfiguration)))
                {
                    throw new ArgumentException("No rdsworm resource type options provided.", nameof(resourceTypeOptions));
                }

                return new RdsS3WormResourceType((RdsS3WormResourceTypeConfiguration)resourceTypeOptions);

            default:
                throw new NotImplementedException();
        }
    }

    public BaseResourceType GetResourceType(Resource resource)
    {
        var config = GetResourceTypeConfig(resource);

        var resourceTypeModel = new ResourceTypeModel();

        var resourceType = resourceTypeModel.GetById(resource.TypeId);

        if (resourceType is null)
        {
            throw new KeyNotFoundException("No resource type found, for the given resource.");
        }

        return GetResourceType(resourceType.Type, config);
    }

    public BaseResourceType GetResourceType(string type, string specificType, GetResourceTypeConfigOptions? getResourceTypeConfigOptions = null)
    {
        if (string.IsNullOrWhiteSpace(type))
        {
            throw new ArgumentException($"'{nameof(type)}' cannot be null or whitespace.", nameof(type));
        }

        if (string.IsNullOrWhiteSpace(specificType))
        {
            throw new ArgumentException($"'{nameof(specificType)}' cannot be null or whitespace.", nameof(specificType));
        }

        var config = GetResourceTypeConfig(type, specificType, getResourceTypeConfigOptions);

        return GetResourceType(type, config);
    }

    public ResourceTypeConfiguration GetResourceTypeConfig(Resource resource)
    {
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        var resourceTypeOptionId = resource.ResourceTypeOptionId;

        if (resourceTypeOptionId is null)
        {
            throw new KeyNotFoundException("No resource type found for the given resource.");
        }

        var resourceTypeModel = new ResourceTypeModel();

        var resourceType = resourceTypeModel.GetById(resource.TypeId);

        if (resourceType is null)
        {
            throw new KeyNotFoundException("No resource type found for the given resource.");
        }

        switch (resourceType.Type.ToLower())
        {
            case "gitlab":
                {
                    var gitlabResourceType = new GitlabResourceTypeModel().GetById(resourceTypeOptionId.Value);

                    if (gitlabResourceType is null)
                    {
                        throw new KeyNotFoundException("No gitlab resource type found for the given resource.");
                    }

                    if (!_specificTypes.ContainsKey(resourceType.SpecificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    return new GitLabResourceTypeConfiguration
                    {
                        RepoUrl = gitlabResourceType.RepoUrl,
                        ProjectId = gitlabResourceType.GitlabProjectId,
                        AccessToken = gitlabResourceType.ProjectAccessToken,
                        SpecificType = _specificTypes[resourceType.SpecificType],
                        Branch = gitlabResourceType.Branch,
                        TosAccepted = gitlabResourceType.TosAccepted
                    };
                }

            case "linked":
                {
                    if (!_specificTypes.ContainsKey(resourceType.SpecificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var configuration = new ConsulConfiguration();

                    return new LinkedResourceTypeConfiguration
                    {
                        EpicPrefix = configuration.GetString("coscine/global/epic/prefix"),
                        SparqlEndpoint = configuration.GetString("coscine/local/virtuoso/additional/url"),
                        SpecificType = _specificTypes[resourceType.SpecificType]
                    };
                }

            case "rds":
                {
                    var rdsResourceType = new RDSResourceTypeModel().GetById(resourceTypeOptionId.Value);

                    if (rdsResourceType is null)
                    {
                        throw new KeyNotFoundException("No rds resource type found for the given resource.");
                    }

                    if (!_specificTypes.ContainsKey(resourceType.SpecificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var keyPrefix = _specificTypes[resourceType.SpecificType].Config?.RdsKey;

                    var configuration = new ConsulConfiguration();

                    var ecsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{keyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{keyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{keyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{keyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{keyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{keyPrefix}/replication_group_id"),
                    };

                    return new RdsResourceTypeConfiguration
                    {
                        EcsManagerConfiguration = ecsManagerConfiguration,
                        AccessKey = rdsResourceType.AccessKey,
                        SecretKey = rdsResourceType.SecretKey,
                        Endpoint = rdsResourceType.Endpoint,
                        SpecificType = _specificTypes[resourceType.SpecificType],
                        HttpClientFactory = HttpClientFactory
                    };
                }

            case "rdss3":
                {
                    var rdsS3ResourceType = new RdsS3ResourceTypeModel().GetById(resourceTypeOptionId.Value);

                    if (rdsS3ResourceType is null)
                    {
                        throw new KeyNotFoundException("No rds s3 resource type found for the given resource.");
                    }

                    var configuration = new ConsulConfiguration();

                    if (!_specificTypes.ContainsKey(resourceType.SpecificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var ecsKeyPrefix = _specificTypes[resourceType.SpecificType].Config?.Rdss3Key;
                    var userKeyPrefix = _specificTypes[resourceType.SpecificType].Config?.UserKey;

                    var rdsS3EcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{ecsKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{ecsKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{ecsKeyPrefix}/replication_group_id"),
                    };

                    var userEcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{userKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{userKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{userKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{userKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{userKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{userKeyPrefix}/replication_group_id"),
                    };

                    return new RdsS3ResourceTypeConfiguration
                    {
                        RdsS3EcsManagerConfiguration = rdsS3EcsManagerConfiguration,
                        UserEcsManagerConfiguration = userEcsManagerConfiguration,
                        AccessKey = rdsS3ResourceType.AccessKey,
                        SecretKey = rdsS3ResourceType.SecretKey,
                        AccessKeyRead = rdsS3ResourceType.AccessKeyRead,
                        SecretKeyRead = rdsS3ResourceType.SecretKeyRead,
                        AccessKeyWrite = rdsS3ResourceType.AccessKeyWrite,
                        SecretKeyWrite = rdsS3ResourceType.SecretKeyWrite,
                        Endpoint = rdsS3ResourceType.Endpoint,
                        SpecificType = _specificTypes[resourceType.SpecificType],
                        HttpClientFactory = HttpClientFactory
                    };
                }

            case "rdss3worm":
                {
                    var rdsS3WormResourceType = new RdsS3WormResourceTypeModel().GetById(resourceTypeOptionId.Value);

                    if (rdsS3WormResourceType is null)
                    {
                        throw new KeyNotFoundException("No rds s3 worm resource type found for the given resource.");
                    }

                    var configuration = new ConsulConfiguration();

                    if (!_specificTypes.ContainsKey(resourceType.SpecificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var ecsKeyPrefix = _specificTypes[resourceType.SpecificType].Config?.Rdss3Key;
                    var userKeyPrefix = _specificTypes[resourceType.SpecificType].Config?.UserKey;

                    var rdsS3EcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{ecsKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{ecsKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{ecsKeyPrefix}/replication_group_id"),
                    };

                    var userEcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{userKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{userKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{userKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{userKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{userKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{userKeyPrefix}/replication_group_id"),
                    };

                    return new RdsS3WormResourceTypeConfiguration
                    {
                        RdsS3EcsManagerConfiguration = rdsS3EcsManagerConfiguration,
                        UserEcsManagerConfiguration = userEcsManagerConfiguration,
                        AccessKey = rdsS3WormResourceType.AccessKey,
                        SecretKey = rdsS3WormResourceType.SecretKey,
                        AccessKeyRead = rdsS3WormResourceType.AccessKeyRead,
                        SecretKeyRead = rdsS3WormResourceType.SecretKeyRead,
                        AccessKeyWrite = rdsS3WormResourceType.AccessKeyWrite,
                        SecretKeyWrite = rdsS3WormResourceType.SecretKeyWrite,
                        Endpoint = rdsS3WormResourceType.Endpoint,
                        SpecificType = _specificTypes[resourceType.SpecificType],
                        HttpClientFactory = HttpClientFactory
                    };
                }

            default:
                throw new NotImplementedException();
        }
    }

    public ResourceTypeConfiguration GetResourceTypeConfig(string type, string specificType, GetResourceTypeConfigOptions? getResourceTypeConfigOptions = null)
    {
        if (string.IsNullOrWhiteSpace(type))
        {
            throw new ArgumentException($"'{nameof(type)}' cannot be null or whitespace.", nameof(type));
        }

        if (string.IsNullOrWhiteSpace(specificType))
        {
            throw new ArgumentException($"'{nameof(specificType)}' cannot be null or whitespace.", nameof(specificType));
        }

        switch (type.ToLower())
        {
            case "gitlab":
                {
                    if (getResourceTypeConfigOptions is not null && !getResourceTypeConfigOptions.GetType().IsAssignableFrom(typeof(GetGitLabResourceTypeConfigOptions)))
                    {
                        throw new KeyNotFoundException("No gitlab resource type config options provided.");
                    }

                    if (!_specificTypes.ContainsKey(specificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    if (getResourceTypeConfigOptions is null)
                    {
                        return new GitLabResourceTypeConfiguration
                        {
                            SpecificType = _specificTypes[specificType]
                        };
                    }
                    else
                    {
                        var gitlabConfig = (GetGitLabResourceTypeConfigOptions)getResourceTypeConfigOptions;

                        return new GitLabResourceTypeConfiguration
                        {
                            RepoUrl = gitlabConfig.RepoUrl,
                            ProjectId = gitlabConfig.ProjectId,
                            AccessToken = gitlabConfig.AccessToken,
                            SpecificType = _specificTypes[specificType],
                            Branch = gitlabConfig.Branch,
                            TosAccepted = gitlabConfig.TosAccepted
                        };
                    }
                }
            case "linked":
                {
                    if (!_specificTypes.ContainsKey(specificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var configuration = new ConsulConfiguration();

                    return new LinkedResourceTypeConfiguration
                    {
                        EpicPrefix = configuration.GetString("coscine/global/epic/prefix"),
                        SparqlEndpoint = configuration.GetString("coscine/local/virtuoso/additional/url"),
                        SpecificType = _specificTypes[specificType]
                    };
                }

            case "rds":
                {
                    if (!_specificTypes.ContainsKey(specificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var configuration = new ConsulConfiguration();

                    var keyPrefix = _specificTypes[specificType].Config?.RdsKey;

                    var ecsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{keyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{keyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{keyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{keyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{keyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{keyPrefix}/replication_group_id"),
                    };

                    return new RdsResourceTypeConfiguration
                    {
                        EcsManagerConfiguration = ecsManagerConfiguration,
                        AccessKey = configuration.GetString($"{keyPrefix}/object_user_name"),
                        SecretKey = configuration.GetString($"{keyPrefix}/object_user_secretkey"),
                        Endpoint = configuration.GetString($"{keyPrefix}/s3_endpoint"),
                        SpecificType = _specificTypes[specificType],
                        HttpClientFactory = HttpClientFactory
                    };
                }

            case "rdss3":
                {
                    if (getResourceTypeConfigOptions is not null && !getResourceTypeConfigOptions.GetType().IsAssignableFrom(typeof(GetRdsResourceTypeConfigOptions)))
                    {
                        throw new KeyNotFoundException("No rdss3 resource type config options provided.");
                    }

                    if (!_specificTypes.ContainsKey(specificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var ecsKeyPrefix = _specificTypes[specificType].Config?.Rdss3Key;
                    var userKeyPrefix = _specificTypes[specificType].Config?.UserKey;

                    var configuration = new ConsulConfiguration();

                    var rdsS3EcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{ecsKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{ecsKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{ecsKeyPrefix}/replication_group_id"),
                    };

                    var userEcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{userKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{userKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{userKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{userKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{userKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{userKeyPrefix}/replication_group_id"),
                    };

                    if (getResourceTypeConfigOptions is null)
                    {
                        return new RdsS3ResourceTypeConfiguration
                        {
                            RdsS3EcsManagerConfiguration = rdsS3EcsManagerConfiguration,
                            UserEcsManagerConfiguration = userEcsManagerConfiguration,
                            AccessKey = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                            SecretKey = configuration.GetString($"{ecsKeyPrefix}/object_user_secretkey"),
                            AccessKeyRead = null,
                            SecretKeyRead = RandomHelper.GenerateRandomChunk(32),
                            AccessKeyWrite = null,
                            SecretKeyWrite = RandomHelper.GenerateRandomChunk(32),
                            Endpoint = configuration.GetString($"{ecsKeyPrefix}/s3_endpoint"),
                            SpecificType = _specificTypes[specificType],
                            HttpClientFactory = HttpClientFactory
                        };
                    }
                    else
                    {
                        var config = (GetRdsResourceTypeConfigOptions)getResourceTypeConfigOptions;

                        return new RdsS3ResourceTypeConfiguration
                        {
                            RdsS3EcsManagerConfiguration = rdsS3EcsManagerConfiguration,
                            UserEcsManagerConfiguration = userEcsManagerConfiguration,
                            AccessKey = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                            SecretKey = configuration.GetString($"{ecsKeyPrefix}/object_user_secretkey"),
                            AccessKeyRead = $"read_{config?.Bucketname}",
                            SecretKeyRead = RandomHelper.GenerateRandomChunk(32),
                            AccessKeyWrite = $"write_{config?.Bucketname}",
                            SecretKeyWrite = RandomHelper.GenerateRandomChunk(32),
                            Endpoint = configuration.GetString($"{ecsKeyPrefix}/s3_endpoint"),
                            SpecificType = _specificTypes[specificType],
                            HttpClientFactory = HttpClientFactory
                        };
                    }
                }

            case "rdss3worm":
                {
                    if (getResourceTypeConfigOptions is not null && !getResourceTypeConfigOptions.GetType().IsAssignableFrom(typeof(GetRdsResourceTypeConfigOptions)))
                    {
                        throw new KeyNotFoundException("No rdss3 resource type config options provided.");
                    }

                    if (!_specificTypes.ContainsKey(specificType))
                    {
                        throw new KeyNotFoundException("No specific resource type found for the given resource.");
                    }

                    var ecsKeyPrefix = _specificTypes[specificType].Config?.Rdss3Key;
                    var userKeyPrefix = _specificTypes[specificType].Config?.UserKey;

                    var configuration = new ConsulConfiguration();

                    var rdsS3EcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{ecsKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{ecsKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{ecsKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{ecsKeyPrefix}/replication_group_id"),
                    };

                    var userEcsManagerConfiguration = new EcsManagerConfiguration
                    {
                        ManagerApiEndpoint = configuration.GetString($"{userKeyPrefix}/manager_api_endpoint"),
                        NamespaceName = configuration.GetString($"{userKeyPrefix}/namespace_name"),
                        NamespaceAdminName = configuration.GetString($"{userKeyPrefix}/namespace_admin_name"),
                        NamespaceAdminPassword = configuration.GetString($"{userKeyPrefix}/namespace_admin_password"),
                        ObjectUserName = configuration.GetString($"{userKeyPrefix}/object_user_name"),
                        ReplicationGroupId = configuration.GetString($"{userKeyPrefix}/replication_group_id"),
                    };

                    if (getResourceTypeConfigOptions is null)
                    {
                        return new RdsS3WormResourceTypeConfiguration
                        {
                            RdsS3EcsManagerConfiguration = rdsS3EcsManagerConfiguration,
                            UserEcsManagerConfiguration = userEcsManagerConfiguration,
                            AccessKey = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                            SecretKey = configuration.GetString($"{ecsKeyPrefix}/object_user_secretkey"),
                            AccessKeyRead = null,
                            SecretKeyRead = RandomHelper.GenerateRandomChunk(32),
                            AccessKeyWrite = null,
                            SecretKeyWrite = RandomHelper.GenerateRandomChunk(32),
                            Endpoint = configuration.GetString($"{ecsKeyPrefix}/s3_endpoint"),
                            SpecificType = _specificTypes[specificType],
                            HttpClientFactory = HttpClientFactory
                        };
                    }
                    else
                    {
                        var config = (GetRdsResourceTypeConfigOptions)getResourceTypeConfigOptions;

                        return new RdsS3WormResourceTypeConfiguration
                        {
                            RdsS3EcsManagerConfiguration = rdsS3EcsManagerConfiguration,
                            UserEcsManagerConfiguration = userEcsManagerConfiguration,
                            AccessKey = configuration.GetString($"{ecsKeyPrefix}/object_user_name"),
                            SecretKey = configuration.GetString($"{ecsKeyPrefix}/object_user_secretkey"),
                            AccessKeyRead = $"read_{config?.Bucketname}",
                            SecretKeyRead = RandomHelper.GenerateRandomChunk(32),
                            AccessKeyWrite = $"write_{config?.Bucketname}",
                            SecretKeyWrite = RandomHelper.GenerateRandomChunk(32),
                            Endpoint = configuration.GetString($"{ecsKeyPrefix}/s3_endpoint"),
                            SpecificType = _specificTypes[specificType],
                            HttpClientFactory = HttpClientFactory
                        };
                    }
                }

            default:
                throw new NotImplementedException();
        }
    }

    public void SaveResourceTypeToDatabase(string type, ResourceTypeConfiguration resourceTypeConfiguration, Resource resource)
    {
        var resourceModel = new ResourceModel();

        switch (type.ToLower())
        {
            case "gitlab":
                {
                    var gitlabResourceTypeModel = new GitlabResourceTypeModel();
                    var gitLabResourceTypeConfiguration = resourceTypeConfiguration as GitLabResourceTypeConfiguration;

                    if (resource.ResourceTypeOptionId != null)
                    {
                        var gitlabResourceType = gitlabResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);

                        gitlabResourceType.Branch = gitLabResourceTypeConfiguration?.Branch;
                        gitlabResourceType.ProjectAccessToken = gitLabResourceTypeConfiguration?.AccessToken;
                        gitlabResourceType.GitlabProjectId = gitLabResourceTypeConfiguration is null ? -1 : gitLabResourceTypeConfiguration.ProjectId;
                        gitlabResourceType.RepoUrl = gitLabResourceTypeConfiguration?.RepoUrl;
                        gitlabResourceType.TosAccepted = gitLabResourceTypeConfiguration is not null ? gitLabResourceTypeConfiguration.TosAccepted : false;

                        gitlabResourceTypeModel.Update(gitlabResourceType);
                    }
                    else
                    {
                        ;

                        var gitlabResourceType = new GitlabResourceType()
                        {
                            Branch = gitLabResourceTypeConfiguration?.Branch,
                            ProjectAccessToken = gitLabResourceTypeConfiguration?.AccessToken,
                            GitlabProjectId = gitLabResourceTypeConfiguration is null ? -1 : gitLabResourceTypeConfiguration.ProjectId,
                            RepoUrl = gitLabResourceTypeConfiguration?.RepoUrl,
                            TosAccepted = gitLabResourceTypeConfiguration is not null ? gitLabResourceTypeConfiguration.TosAccepted : false
                        };

                        gitlabResourceTypeModel.Insert(gitlabResourceType);
                        resource.ResourceTypeOptionId = gitlabResourceType.Id;
                        resourceModel.Update(resource);
                    }

                    break;
                }
            case "linked":
                {
                    var linkedResourceTypeModel = new LinkedResourceTypeModel();
                    if (resource.ResourceTypeOptionId == null)
                    {
                        Database.DataModel.LinkedResourceType linkedResourceType = new();

                        linkedResourceTypeModel.Insert(linkedResourceType);
                        resource.ResourceTypeOptionId = linkedResourceType.Id;
                        resourceModel.Update(resource);
                    }

                    break;
                }

            case "rds":
                {
                    var rdsResourceTypeModel = new RDSResourceTypeModel();

                    if (resource.ResourceTypeOptionId != null)
                    {
                        var rdsResourceType = rdsResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                        rdsResourceTypeModel.Update(rdsResourceType);
                    }
                    else
                    {
                        var rdsResourceTypeConfiguration = resourceTypeConfiguration as RdsResourceTypeConfiguration;

                        var rdsResourceType = new RdsresourceType()
                        {
                            BucketName = resource.Id.ToString(),
                            AccessKey = rdsResourceTypeConfiguration?.AccessKey,
                            SecretKey = rdsResourceTypeConfiguration?.SecretKey,
                            Endpoint = rdsResourceTypeConfiguration?.Endpoint
                        };

                        rdsResourceTypeModel.Insert(rdsResourceType);
                        resource.ResourceTypeOptionId = rdsResourceType.Id;
                        resourceModel.Update(resource);
                    }

                    break;
                }

            case "rdss3":
                {
                    var rdsS3ResourceTypeModel = new RdsS3ResourceTypeModel();

                    if (resource.ResourceTypeOptionId != null)
                    {
                        var rdsS3ResourceType = rdsS3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                        rdsS3ResourceTypeModel.Update(rdsS3ResourceType);
                    }
                    else
                    {
                        var rdsS3ResourceTypeConfiguration = resourceTypeConfiguration as RdsS3ResourceTypeConfiguration;

                        var rdsS3ResourceType = new RdsS3resourceType()
                        {
                            BucketName = resource.Id.ToString(),
                            AccessKey = rdsS3ResourceTypeConfiguration?.AccessKey,
                            SecretKey = rdsS3ResourceTypeConfiguration?.SecretKey,
                            AccessKeyRead = rdsS3ResourceTypeConfiguration?.AccessKeyRead,
                            SecretKeyRead = rdsS3ResourceTypeConfiguration?.SecretKeyRead,
                            AccessKeyWrite = rdsS3ResourceTypeConfiguration?.AccessKeyWrite,
                            SecretKeyWrite = rdsS3ResourceTypeConfiguration?.SecretKeyWrite,
                            Endpoint = rdsS3ResourceTypeConfiguration?.Endpoint
                        };

                        rdsS3ResourceTypeModel.Insert(rdsS3ResourceType);
                        resource.ResourceTypeOptionId = rdsS3ResourceType.Id;
                        resourceModel.Update(resource);
                    }
                    break;
                }

            case "rdss3worm":
                {
                    var rdsS3WormResourceTypeModel = new RdsS3WormResourceTypeModel();

                    if (resource.ResourceTypeOptionId != null)
                    {
                        var rdsS3ResourceType = rdsS3WormResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                        rdsS3WormResourceTypeModel.Update(rdsS3ResourceType);
                    }
                    else
                    {
                        var rdsS3WormResourceTypeConfiguration = resourceTypeConfiguration as RdsS3WormResourceTypeConfiguration;

                        var rdsS3WormesourceType = new RdsS3wormResourceType()
                        {
                            BucketName = resource.Id.ToString(),
                            AccessKey = rdsS3WormResourceTypeConfiguration?.AccessKey,
                            SecretKey = rdsS3WormResourceTypeConfiguration?.SecretKey,
                            AccessKeyRead = rdsS3WormResourceTypeConfiguration?.AccessKeyRead,
                            SecretKeyRead = rdsS3WormResourceTypeConfiguration?.SecretKeyRead,
                            AccessKeyWrite = rdsS3WormResourceTypeConfiguration?.AccessKeyWrite,
                            SecretKeyWrite = rdsS3WormResourceTypeConfiguration?.SecretKeyWrite,
                            Endpoint = rdsS3WormResourceTypeConfiguration?.Endpoint
                        };

                        rdsS3WormResourceTypeModel.Insert(rdsS3WormesourceType);
                        resource.ResourceTypeOptionId = rdsS3WormesourceType.Id;
                        resourceModel.Update(resource);
                    }
                    break;
                }

            default:
                throw new NotImplementedException();
        }
    }

    [Obsolete("GetResourceTypes() is deprecated, please use GetSpecificResourceTypes(ResourceTypeStatus status) instead.")]
    public IEnumerable<string> GetResourceTypes()
    {
        return _specificTypes.Where(x => x.Value.Status == ResourceTypeStatus.Active).Select(x => x.Key);
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        return _specificTypes.Select(x => x.Value);
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes().Where(x => x.Status == status);
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return GetSpecificResourceTypes().Where(x => x.SupportedOrganizations is not null
            && (x.SupportedOrganizations.Contains(OrganizationRor) || x.SupportedOrganizations.Contains("*")));
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes(OrganizationRor).Where(x => x.Status == status);
    }
}