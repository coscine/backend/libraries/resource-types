﻿using System.Security.Cryptography;
using System.Text;

namespace Coscine.ResourceTypes;

public static class RandomHelper
{
    private const string humanSafeChars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";
    private const string urlSafeChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private const char separatorChar = '-';
    private static readonly RandomNumberGenerator rng = RandomNumberGenerator.Create();

    public static string GenerateRandomChunk(int length, int chunks = 1, bool humanSafe = false)
    {
        char[] chars = (humanSafe ? humanSafeChars : urlSafeChars).ToCharArray();
        byte[] randomBytes = new byte[chunks * length];

        lock (rng)
        {
            rng.GetBytes(randomBytes);
        }

        var code = new StringBuilder();
        for (int i = 0; i < randomBytes.Length; i++)
        {
            if (i % length == 0)
            {
                code.Append(separatorChar);
            }
            code.Append(chars[randomBytes[i] % chars.Length]);
        }

        code.Remove(0, 1);

        return code.ToString();
    }
}