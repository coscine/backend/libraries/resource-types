﻿using Coscine.Configuration;
using Coscine.ResourceTypes.GitLab;
using Coscine.ResourceTypes.ResourceTypeConfigs;
using GitLabApiClient;
using GitLabApiClient.Models.Projects.Responses;
using NUnit.Framework;

namespace Coscine.ResourceTypes.Tests
{
    [TestFixture]
    public class GitLabResourceTypeTests
    {
        // --------------------------------------------------------------------------------------
        // USING THIS PROJECT: https://git.rwth-aachen.de/coscine/x/tools/gitlab-api-test-project
        // --------------------------------------------------------------------------------------
        private readonly ConsulConfiguration _configuration = new();

        private readonly string _gitLabRepoUrl = "https://git.rwth-aachen.de";
        private readonly int _gitLabProjectId = 74627;
        private readonly string _type = "gitlab";
        private string _gitLabAccessToken = null!;
        private string _gitLabBranchName = null!;
        private string _guid = null!;

        private static readonly IEnumerable<string> _specificTypes = new List<string>
        {
            "gitlab",
        };

        private const string _specialChars = " (TeSt).,_-+";

        private static IEnumerable<object[]> GetTestConfigs()
        {
            foreach (var specificType in _specificTypes)
            {
                yield return new object[] { specificType };
            }
        }

        private GitLabClient _gitLabClient = null!;
        private Project _gitLabProject = null!;

        [SetUp]
        public void SetUp()
        {
            _guid = Guid.NewGuid().ToString();
            _gitLabBranchName = $"Coscine-GitLabResourceType-Tests-{Guid.NewGuid().ToString()[..8]}";
            _gitLabAccessToken = _configuration.GetStringAndWait("coscine/global/gitlab-resource/api-token");

            _gitLabClient = new GitLabClient(new Uri(_gitLabRepoUrl).ToString(), _gitLabAccessToken);
            _gitLabProject = _gitLabClient.Projects.GetAsync(_gitLabProjectId).Result;
            _gitLabClient.Branches.CreateAsync(_gitLabProject.Id, new GitLabApiClient.Models.Branches.Requests.CreateBranchRequest(_gitLabBranchName, _gitLabProject.DefaultBranch)).Wait();
        }

        [TearDown]
        public void TearDown()
        {
            _gitLabClient.Branches.DeleteBranch(_gitLabProject, _gitLabBranchName).Wait();
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestConstructor(string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetGitLabResourceTypeConfigOptions
            {
                RepoUrl = _gitLabRepoUrl,
                ProjectId = _gitLabProjectId,
                AccessToken = _gitLabAccessToken,
                Branch = _gitLabBranchName
            });
            Assert.NotNull(resourceType);
            Assert.True(resourceType.GetType() == typeof(GitLabResourceType));
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestCreate(string specificType)
        {
            var resourceType = (GitLabResourceType)ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetGitLabResourceTypeConfigOptions
            {
                RepoUrl = _gitLabRepoUrl,
                ProjectId = _gitLabProjectId,
                AccessToken = _gitLabAccessToken,
                Branch = _gitLabBranchName
            });
            resourceType.CreateResource(_guid).Wait();

            var randomFileName = $"{Guid.NewGuid().ToString()[..8]}{_specialChars}.txt";

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, randomFileName, memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", $"Coscine-GitLabResourceType-Tests: StoreEntry {randomFileName}" }
            }).Wait();

            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, randomFileName, memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", $"Coscine-GitLabResourceType-Tests: StoreEntry {randomFileName}" }
            }).Wait();

            var entry = resourceType.GetEntry(_guid, randomFileName).Result;

            Assert.NotNull(entry);
            Assert.AreEqual(entry?.BodyBytes, testData.Length);
            Assert.AreEqual(entry?.Key, randomFileName);

            var entries = resourceType.ListEntries(_guid, "").Result;

            Assert.NotNull(entries);
            Assert.IsNotEmpty(entries);
            Assert.IsTrue(entries.Count >= 1);
            Assert.IsTrue(entries.Any(e => e.Key.Equals(randomFileName)));

            resourceType.DeleteEntry(_guid, entry!.Key, new Dictionary<string, string>()
            {
                { "commit_message", $"Coscine-GitLabResourceType-Tests: DeleteEntry {randomFileName}" }
            }).Wait();
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestList(string specificType)
        {
            var folders = new List<string> { "Folder A", "Folder B", "SubFolder C" };
            var files = new List<string> { "File A", "File B", "File C", "File D" };

            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            var resourceType = (GitLabResourceType)ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetGitLabResourceTypeConfigOptions
            {
                RepoUrl = _gitLabRepoUrl,
                ProjectId = _gitLabProjectId,
                AccessToken = _gitLabAccessToken,
                Branch = _gitLabBranchName
            });

            var memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, "File A.txt", memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", "Coscine-GitLabResourceType-Tests: StoreEntry File A.txt" }
            }).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, "File B.txt", memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", "Coscine-GitLabResourceType-Tests: StoreEntry File B.txt" }
            }).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, "Folder A/File C.txt", memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", "Coscine-GitLabResourceType-Tests: StoreEntry Folder A/File C.txt" }
            }).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, "Folder B/File D.txt", memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", "Coscine-GitLabResourceType-Tests: StoreEntry Folder B/File D.txt" }
            }).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, "Folder B/File E.txt", memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", "Coscine-GitLabResourceType-Tests: StoreEntry Folder B/File E.txt" }
            }).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_guid, "Folder B/SubFolder F/File G.txt", memoryStream, new Dictionary<string, string>()
            {
                { "commit_message", "Coscine-GitLabResourceType-Tests: StoreEntry Folder B/SubFolder F/File G.txt.txt" }
            }).Wait();

            var entries = resourceType.ListEntries(_guid, "File C").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Assert.True(entries.Count == 0);

            Console.WriteLine("---------------");

            entries = resourceType.ListEntries(_guid, "").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Assert.True(entries.Count >= 4);
            Assert.True(entries.Any(x => x.Key == "File A.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "File B.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder A/" && !x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/" && !x.HasBody));

            Console.WriteLine("---------------");

            entries = resourceType.ListEntries(_guid, "Folder A/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Assert.True(entries.Count == 1);
            Assert.True(entries.Any(x => x.Key == "Folder A/File C.txt" && x.HasBody));

            Console.WriteLine("---------------");

            entries = resourceType.ListEntries(_guid, "Folder B/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Assert.True(entries.Count == 3);
            Assert.True(entries.Any(x => x.Key == "Folder B/File D.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/File E.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/SubFolder F/" && !x.HasBody));

            Console.WriteLine("---------------");
        }
    }
}