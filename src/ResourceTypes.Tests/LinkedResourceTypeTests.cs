﻿using Coscine.ResourceTypes.Base;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Text;

namespace Coscine.ResourceTypes.Tests
{
    [TestFixture]
    public class LinkedResourceTypeTests
    {
        private BaseResourceType _resourceType = null!;
        private readonly Guid id = Guid.NewGuid();
        private readonly string key = "/test.txt";

        private static string? StreamToString(Stream? stream)
        {
            if (stream == null)
            {
                return null;
            }

            using var reader = new StreamReader(stream, Encoding.UTF8);
            return reader.ReadToEnd();
        }

        private static Stream StringToStream(string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            return new MemoryStream(byteArray);
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _resourceType = ResourceTypeFactory.Instance.GetResourceType("linked", "linked");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            foreach (var g in _resourceType.ListEntries(id.ToString(), "/test").Result)
            {
                _resourceType.DeleteEntry(id.ToString(), g.Key).Wait();
            }

            // Should the list feature no work
            if (_resourceType.LoadEntry(id.ToString(), key).Result != null)
            {
                _resourceType.DeleteEntry(id.ToString(), key).Wait();
            }

            if (_resourceType.LoadEntry(id.ToString(), "/testA.txt").Result != null)
            {
                _resourceType.DeleteEntry(id.ToString(), "/testA.txt").Wait();
            }

            if (_resourceType.LoadEntry(id.ToString(), "/testB.txt").Result != null)
            {
                _resourceType.DeleteEntry(id.ToString(), "/testB.txt").Wait();
            }
        }

        [Test]
        public async Task ListEntries()
        {
            const string textA = "TestText A";
            const string textB = "TestText B ÄÖÜ";

            Assert.True((await _resourceType.ListEntries(id.ToString(), "/test")).Count == 0);
            await _resourceType.StoreEntry(id.ToString(), "/testA.txt", StringToStream(textA));
            await _resourceType.StoreEntry(id.ToString(), "/testB.txt", StringToStream(textB));
            Assert.True((await _resourceType.ListEntries(id.ToString(), "/test")).Count == 2);
            await _resourceType.DeleteEntry(id.ToString(), "/testA.txt");
            await _resourceType.DeleteEntry(id.ToString(), "/testB.txt");
            Assert.True((await _resourceType.ListEntries(id.ToString(), "/test")).Count == 0);
        }

        [Test]
        public async Task CompleteTest()
        {
            const string textA = "TestText A";
            const string textB = "TestText B ÄÖÜ";

            // Max size/length
            const int bufferSize = 1024 * 4;

            await _resourceType.StoreEntry(id.ToString(), key, StringToStream(textA));
            Assert.True(StreamToString(await _resourceType.LoadEntry(id.ToString(), key)) == textA);
            Assert.True((await _resourceType.GetEntry(id.ToString(), key))?.BodyBytes == Encoding.UTF8.GetBytes(textA).Length);

            await _resourceType.StoreEntry(id.ToString(), key, StringToStream(textB));
            Assert.True(StreamToString(await _resourceType.LoadEntry(id.ToString(), key)) == textB);
            Assert.True((await _resourceType.GetEntry(id.ToString(), key))?.BodyBytes == Encoding.UTF8.GetBytes(textB).Length);

            byte[] buffer = new byte[bufferSize];
            var random = new Random();
            random.NextBytes(buffer);
            // For saving binary data to a string, UTF8 and string escaping is needed. This will increase the size!
            buffer = Encoding.UTF8.GetBytes(Uri.EscapeDataString(Encoding.UTF8.GetString(buffer)));

            // Resize back to the maximum Size
            Array.Resize(ref buffer, bufferSize);

            byte[] buffer2 = new byte[buffer.Length];

            await _resourceType.StoreEntry(id.ToString(), key, new MemoryStream(buffer));
            Assert.True((await _resourceType.GetEntry(id.ToString(), key))?.BodyBytes == buffer.Length);
            var read = (await _resourceType.LoadEntry(id.ToString(), key))?.Read(buffer2, 0, buffer2.Length);
            Assert.True(read == buffer.Length);
            Assert.True(buffer.SequenceEqual(buffer2));

            await _resourceType.DeleteEntry(id.ToString(), key);
            Assert.True(await _resourceType.LoadEntry(id.ToString(), key) == null);
            Assert.True(await _resourceType.GetEntry(id.ToString(), key) == null);
        }

        [Test]
        public void MaxSize()
        {
            byte[] buffer = new byte[(4 * 1024) + 1];
            var random = new Random();
            random.NextBytes(buffer);

            Assert.Throws<Exception>(() => _resourceType.StoreEntry(id.ToString(), key, new MemoryStream(buffer)).Wait());
        }

        [Test]
        public void TestResourceTypeInformation()
        {
            var resourceTypeInformation = _resourceType.GetResourceTypeInformation().Result;

            string jsonString = JsonConvert.SerializeObject(resourceTypeInformation);
            Console.WriteLine(jsonString);

            Assert.IsFalse(resourceTypeInformation.IsQuotaAvailable);
            Assert.IsFalse(resourceTypeInformation.IsQuotaAdjustable);
        }
    }
}