﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Coscine.ECSManager;
using Coscine.ResourceTypes.Base;
using Coscine.ResourceTypes.RdsS3Worm;
using Coscine.ResourceTypes.ResourceTypeConfigs;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Coscine.ResourceTypes.Tests
{
    [TestFixture]
    public class RdsS3WormResourceTypeTests
    {
        private readonly string _testPrefix = "Coscine-RdsS3WormResourceType-Tests";
        private readonly string _type = "rdss3worm";

        private static readonly IEnumerable<string> _specificTypes = new List<string>
        {
            "rdss3wormrwth"
        };

        private const string _specialChars = " ()?+-;.,";

        private static IEnumerable<object[]> GetTestConfigs()
        {
            foreach (var specificType in _specificTypes)
            {
                yield return new object[] { specificType };
            }
        }

        private long _quota;

        private Guid _guid;
        private string _bucketName = null!;

        [SetUp]
        public void SetUp()
        {
            _quota = 1;
            _guid = Guid.NewGuid();
            _bucketName = $"{_testPrefix}.{_guid}";
        }

        private EcsManager CreateRdsManager(string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3WormResourceType;

            return new EcsManager
            {
                EcsManagerConfiguration = resourceType?.RdsS3WormResourceTypeConfiguration.RdsS3EcsManagerConfiguration
            };
        }

        private EcsManager CreateUserManager(string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3WormResourceType;

            return new EcsManager
            {
                EcsManagerConfiguration = resourceType?.RdsS3WormResourceTypeConfiguration.UserEcsManagerConfiguration
            };
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var specificType in _specificTypes)
            {
                var ecsManager = CreateRdsManager(_type, specificType);
#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
                try
                {
                    ClearBucket(_bucketName, _type, specificType);
                    ecsManager.DeleteBucket(_bucketName).Wait();
                }
                catch (Exception)
                {
                }
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.
            }

            var userEcsManager = CreateUserManager(_type, _specificTypes.First());

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, _specificTypes.First(), new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3WormResourceType;
            var readUser = resourceType?.RdsS3WormResourceTypeConfiguration.AccessKeyRead;
            var writeUser = resourceType?.RdsS3WormResourceTypeConfiguration.AccessKeyWrite;

#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
            try
            {
                userEcsManager.DeleteObjectUser(readUser).Wait();
            }
            catch (Exception)
            {
            }
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.

#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
            try
            {
                userEcsManager.DeleteObjectUser(writeUser).Wait();
            }
            catch (Exception)
            {
            }
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestConstructor(string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            Assert.NotNull(resourceType);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestCreate(string specificType)
        {
            const int retention = 20;
            var rdsS3EcsManager = CreateRdsManager(_type, specificType);
            var userEcsManager = CreateUserManager(_type, specificType);

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            var options = new Dictionary<string, string> { { "retention", $"{retention}" } };
            resourceType.CreateResource(_bucketName, _quota, options).Wait();

            var randomFileName = $"{Guid.NewGuid()}{_specialChars}=.txt";

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, randomFileName, memoryStream).Wait();

            var entry = resourceType.GetEntry(_bucketName, randomFileName).Result;

            Assert.NotNull(entry);
            Assert.AreEqual(entry?.BodyBytes, testData.Length);
            Assert.AreEqual(entry?.Key, randomFileName);

            var entries = resourceType.ListEntries(_bucketName, "").Result;

            Assert.NotNull(entries);
            Assert.IsNotEmpty(entries);
            Assert.IsTrue(entries.Count == 1);
            Assert.AreEqual(entries[0].Key, randomFileName);
            Assert.IsTrue(entries[0].Key == entry?.Key);

            var deleteObjectRequest = new DeleteObjectRequest()
            {
                BucketName = _bucketName,
                Key = randomFileName
            };

            var s3Client = GetS3Client(_type, specificType);

            Assert.Throws<AggregateException>(() => s3Client.DeleteObjectAsync(deleteObjectRequest).Wait());
            memoryStream = new MemoryStream();

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            Assert.Throws<AggregateException>(() => resourceType.StoreEntry(_bucketName, randomFileName, memoryStream).Wait());

            bool objectDeleted = false;

            int retries = 0;

            while (!objectDeleted && retries < 5)
            {
                try
                {
                    var url = resourceType.GetPresignedUrl(_bucketName, randomFileName, CoscineHttpVerb.DELETE).Result;
                    var responseMessage = ResourceTypeFactory.HttpClientFactory.CreateClient().DeleteAsync(url).Result;
                    responseMessage.Content.ReadAsStream();
                    objectDeleted = responseMessage.IsSuccessStatusCode;
                }
                catch (Exception)
                {
                    retries++;
                    Thread.Sleep(5000);
                }
            }

            Assert.True(objectDeleted, $"Object was not deleted after {retries} retries.");

            var readUser = (resourceType as RdsS3WormResourceType)?.RdsS3WormResourceTypeConfiguration.AccessKeyRead;
            var writeUser = (resourceType as RdsS3WormResourceType)?.RdsS3WormResourceTypeConfiguration.AccessKeyWrite;

            Assert.True(userEcsManager.DeleteObjectUser(readUser).Result);
            Assert.True(userEcsManager.DeleteObjectUser(writeUser).Result);

            Assert.True(rdsS3EcsManager.DeleteBucket(_bucketName).Result);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestList(string specificType)
        {
            const int retention = 20;
            var folders = new List<string> { "Folder A", "Folder B", "SubFolder C" };
            var files = new List<string> { "File A", "File B", "File C", "File D" };

            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            var rdsS3EcsManager = CreateRdsManager(_type, specificType);
            var userEcsManager = CreateUserManager(_type, specificType);

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            var options = new Dictionary<string, string> { { "retention", $"{retention}" } };
            resourceType.CreateResource(_bucketName, _quota, options).Wait();

            var memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "File A.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "File B.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder A/File C.txt", memoryStream).Wait();
            // Add empty folder
            resourceType.StoreEntry(_bucketName, "Folder A/", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder B/File D.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder B/File E.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder B/SubFolder F/File G.txt", memoryStream).Wait();

            // Add empty folder
            resourceType.StoreEntry(_bucketName, "Folder C/", memoryStream).Wait();

            var entries = resourceType.ListEntries(_bucketName, "File C").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Assert.True(entries.Count == 0);

            Console.WriteLine("---------------");

            entries = resourceType.ListEntries(_bucketName, "").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 5);
            Assert.True(entries.Any(x => x.Key == "File A.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "File B.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder A/" && !x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/" && !x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder C/" && !x.HasBody));

            entries = resourceType.ListEntries(_bucketName, "Folder A/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 1);
            Assert.True(entries.Any(x => x.Key == "Folder A/File C.txt" && x.HasBody));

            entries = resourceType.ListEntries(_bucketName, "Folder B/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 3);
            Assert.True(entries.Any(x => x.Key == "Folder B/File D.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/File E.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/SubFolder F/" && !x.HasBody));

            entries = resourceType.ListEntries(_bucketName, "Folder C/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 0);

            // Make sure, that the objects can be deleted
            Thread.Sleep((retention + 1) * 1000);
            ClearBucket(_bucketName, _type, specificType);

            var readUser = (resourceType as RdsS3WormResourceType)?.RdsS3WormResourceTypeConfiguration.AccessKeyRead;
            var writeUser = (resourceType as RdsS3WormResourceType)?.RdsS3WormResourceTypeConfiguration.AccessKeyWrite;

            Assert.True(userEcsManager.DeleteObjectUser(readUser).Result);
            Assert.True(userEcsManager.DeleteObjectUser(writeUser).Result);

            Assert.True(rdsS3EcsManager.DeleteBucket(_bucketName).Result);
        }

        private void ClearBucket(string bucket, string type, string specificType)
        {
            AmazonS3Client client = GetS3Client(type, specificType);

            // List and delete all objects
            var listObjectsRequest = new ListObjectsRequest
            {
                BucketName = bucket
            };
            {
                ListObjectsResponse listObjectsResponse;
                do
                {
                    // Get a list of objects
                    listObjectsResponse = client.ListObjectsAsync(listObjectsRequest).Result;
                    foreach (S3Object obj in listObjectsResponse.S3Objects)
                    {
                        var deleteObjectRequest = new DeleteObjectRequest()
                        {
                            BucketName = obj.BucketName,
                            Key = obj.Key
                        };

                        client.DeleteObjectAsync(deleteObjectRequest).Wait();
                    }

                    // Set the marker property
                    listObjectsRequest.Marker = listObjectsResponse.NextMarker;
                } while (listObjectsResponse.IsTruncated);
            }

            var transferUtility = new TransferUtility(client);

            // Abort all in-progress uploads initiated before the specified date.
            transferUtility.AbortMultipartUploadsAsync(bucket, DateTime.Now.AddDays(1)).Wait();
        }

        private AmazonS3Client GetS3Client(string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3WormResourceType;

            var amazonS3Config = new AmazonS3Config
            {
                ServiceURL = resourceType?.RdsS3WormResourceTypeConfiguration.Endpoint,
                ForcePathStyle = true
            };

            return new AmazonS3Client(resourceType?.RdsS3WormResourceTypeConfiguration.AccessKey, resourceType?.RdsS3WormResourceTypeConfiguration.SecretKey, amazonS3Config);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestResourceTypeInformation(string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            var resourceTypeInformation = resourceType.GetResourceTypeInformation().Result;

            string jsonString = JsonConvert.SerializeObject(resourceTypeInformation);
            Console.WriteLine(jsonString);

            Assert.IsTrue(resourceTypeInformation.IsQuotaAvailable);
            Assert.IsFalse(resourceTypeInformation.IsQuotaAdjustable);
        }
    }
}