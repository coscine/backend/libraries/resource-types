﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Coscine.ECSManager;
using Coscine.ResourceTypes.RdsS3;
using Coscine.ResourceTypes.ResourceTypeConfigs;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Coscine.ResourceTypes.Tests
{
    [TestFixture]
    public class RdsS3ResourceTypeTests
    {
        private readonly string _testPrefix = "Coscine-RdsS3ResourceType-Tests";

        private readonly string _type = "rdss3";

        private static readonly IEnumerable<string> _specificTypes = new List<string>
        {
            "rdss3rwth",
            "rdss3ude",
            "rdss3nrw",
            "rdss3tudo"
        };

        private const string _specialChars = " ()?+-;.,";

        private static IEnumerable<object[]> GetTestConfigs()
        {
            foreach (var specificType in _specificTypes)
            {
                yield return new object[] { specificType };
            }
        }

        private long _quota;

        private Guid _guid;
        private string _bucketName = null!;

        [SetUp]
        public void SetUp()
        {
            _quota = 1;
            _guid = Guid.NewGuid();
            _bucketName = $"{_testPrefix}.{_guid}";
        }

        private EcsManager CreateRdsManager(string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3ResourceType;

            return new EcsManager
            {
                EcsManagerConfiguration = resourceType?.RdsS3ResourceTypeConfiguration.RdsS3EcsManagerConfiguration
            };
        }

        private EcsManager CreateUserManager(string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3ResourceType;

            return new EcsManager
            {
                EcsManagerConfiguration = resourceType?.RdsS3ResourceTypeConfiguration.UserEcsManagerConfiguration
            };
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var specificType in _specificTypes)
            {
                var ecsManager = CreateRdsManager(_type, specificType);
                try
                {
                    ClearBucket(_bucketName, _type, specificType);
                    ecsManager.DeleteBucket(_bucketName).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            var userEcsManager = CreateUserManager(_type, _specificTypes.First());

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, _specificTypes.First(), new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3ResourceType;
            var readUser = resourceType?.RdsS3ResourceTypeConfiguration.AccessKeyRead;
            var writeUser = resourceType?.RdsS3ResourceTypeConfiguration.AccessKeyWrite;

            try
            {
                userEcsManager.DeleteObjectUser(readUser).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                userEcsManager.DeleteObjectUser(writeUser).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestConstructor(string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            Assert.NotNull(resourceType);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestCreate(string specificType)
        {
            var rdsS3EcsManager = CreateRdsManager(_type, specificType);
            var userEcsManager = CreateUserManager(_type, specificType);

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            resourceType.CreateResource(_bucketName, _quota).Wait();

            var randomFileName = $"{Guid.NewGuid()}{_specialChars}=.txt";

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, randomFileName, memoryStream).Wait();

            var entry = resourceType.GetEntry(_bucketName, randomFileName).Result;

            Assert.NotNull(entry);
            Assert.AreEqual(entry?.BodyBytes, testData.Length);
            Assert.AreEqual(entry?.Key, randomFileName);

            var entries = resourceType.ListEntries(_bucketName, "").Result;

            Assert.NotNull(entries);
            Assert.IsNotEmpty(entries);
            Assert.IsTrue(entries.Count == 1);
            Assert.AreEqual(entries[0].Key, randomFileName);
            Assert.IsTrue(entries[0].Key == entry?.Key);

            resourceType.DeleteEntry(_bucketName, entry!.Key).Wait();

            var readUser = (resourceType as RdsS3ResourceType)?.RdsS3ResourceTypeConfiguration.AccessKeyRead;
            var writeUser = (resourceType as RdsS3ResourceType)?.RdsS3ResourceTypeConfiguration.AccessKeyWrite;

            Assert.True(userEcsManager.DeleteObjectUser(readUser).Result);
            Assert.True(userEcsManager.DeleteObjectUser(writeUser).Result);
            Assert.True(rdsS3EcsManager.DeleteBucket(_bucketName).Result);
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestList(string specificType)
        {
            var folders = new List<string> { "Folder A", "Folder B", "SubFolder C" };
            var files = new List<string> { "File A", "File B", "File C", "File D" };

            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            var rdsS3EcsManager = CreateRdsManager(_type, specificType);
            var userEcsManager = CreateUserManager(_type, specificType);

            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            resourceType.CreateResource(_bucketName, _quota).Wait();

            var memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "File A.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "File B.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder A/File C.txt", memoryStream).Wait();
            // Add empty folder
            resourceType.StoreEntry(_bucketName, "Folder A/", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder B/File D.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder B/File E.txt", memoryStream).Wait();

            memoryStream = new MemoryStream();
            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            resourceType.StoreEntry(_bucketName, "Folder B/SubFolder F/File G.txt", memoryStream).Wait();

            // Add empty folder
            resourceType.StoreEntry(_bucketName, "Folder C/", memoryStream).Wait();

            var entries = resourceType.ListEntries(_bucketName, "File C").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Assert.True(entries.Count == 0);

            Console.WriteLine("---------------");

            entries = resourceType.ListEntries(_bucketName, "").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 5);
            Assert.True(entries.Any(x => x.Key == "File A.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "File B.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder A/" && !x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/" && !x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder C/" && !x.HasBody));

            entries = resourceType.ListEntries(_bucketName, "Folder A/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 1);
            Assert.True(entries.Any(x => x.Key == "Folder A/File C.txt" && x.HasBody));

            entries = resourceType.ListEntries(_bucketName, "Folder B/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 3);
            Assert.True(entries.Any(x => x.Key == "Folder B/File D.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/File E.txt" && x.HasBody));
            Assert.True(entries.Any(x => x.Key == "Folder B/SubFolder F/" && !x.HasBody));

            entries = resourceType.ListEntries(_bucketName, "Folder C/").Result;

            foreach (var obj in entries)
            {
                Console.WriteLine(obj.Key);
            }

            Console.WriteLine("---------------");

            Assert.True(entries.Count == 0);

            ClearBucket(_bucketName, _type, specificType);

            var readUser = (resourceType as RdsS3ResourceType)?.RdsS3ResourceTypeConfiguration.AccessKeyRead;
            var writeUser = (resourceType as RdsS3ResourceType)?.RdsS3ResourceTypeConfiguration.AccessKeyWrite;

            Assert.True(userEcsManager.DeleteObjectUser(readUser).Result);
            Assert.True(userEcsManager.DeleteObjectUser(writeUser).Result);

            Assert.True(rdsS3EcsManager.DeleteBucket(_bucketName).Result);
        }

        private void ClearBucket(string bucket, string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3ResourceType;

            AmazonS3Client client = GetS3Client(type, specificType);

            // List and delete all objects
            var listObjectsRequest = new ListObjectsRequest
            {
                BucketName = bucket
            };
            {
                ListObjectsResponse listObjectsResponse;
                do
                {
                    // Get a list of objects
                    listObjectsResponse = client.ListObjectsAsync(listObjectsRequest).Result;
                    foreach (S3Object obj in listObjectsResponse.S3Objects)
                    {
                        resourceType?.DeleteEntry(obj.BucketName, obj.Key);
                    }

                    // Set the marker property
                    listObjectsRequest.Marker = listObjectsResponse.NextMarker;
                } while (listObjectsResponse.IsTruncated);
            }

            var transferUtility = new TransferUtility(client);

            // Abort all in-progress uploads initiated before the specified date.
            transferUtility.AbortMultipartUploadsAsync(bucket, DateTime.Now.AddDays(1)).Wait();
        }

        private AmazonS3Client GetS3Client(string type, string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName }) as RdsS3ResourceType;

            return new AmazonS3Client(resourceType?.RdsS3ResourceTypeConfiguration.AccessKey, resourceType?.RdsS3ResourceTypeConfiguration.SecretKey, new AmazonS3Config
            {
                ServiceURL = resourceType?.RdsS3ResourceTypeConfiguration.Endpoint,
                ForcePathStyle = true
            });
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void TestResourceTypeInformation(string specificType)
        {
            var resourceType = ResourceTypeFactory.Instance.GetResourceType(_type, specificType, new GetRdsResourceTypeConfigOptions { Bucketname = _bucketName });
            var resourceTypeInformation = resourceType.GetResourceTypeInformation().Result;

            string jsonString = JsonConvert.SerializeObject(resourceTypeInformation);
            Console.WriteLine(jsonString);

            Assert.IsTrue(resourceTypeInformation.IsQuotaAvailable);
            Assert.IsTrue(resourceTypeInformation.IsQuotaAdjustable);
        }
    }
}