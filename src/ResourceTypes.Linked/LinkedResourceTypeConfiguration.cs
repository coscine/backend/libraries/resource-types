﻿using Coscine.ResourceTypes.Base;

namespace Coscine.ResourceTypes.Linked;

public class LinkedResourceTypeConfiguration : ResourceTypeConfiguration
{
    [Obsolete]
    public string EpicPrefix { get; set; } = null!;
    public string? SparqlEndpoint { get; set; }
    [Obsolete]
    public string UrlPrefix { get; set; } = "https://hdl.handle.net/";
    [Obsolete]
    public string Predicate { get; set; } = "https://purl.org/coscine/terms/linked#body";
}