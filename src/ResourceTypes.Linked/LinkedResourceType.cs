﻿using Coscine.Metadata;
using Coscine.Metadata.Util;
using Coscine.ResourceTypes.Base;
using Coscine.ResourceTypes.Base.Helpers;
using Coscine.ResourceTypes.Base.Models;
using Newtonsoft.Json;
using System.Text;
using System.Web;
using VDS.RDF;

namespace Coscine.ResourceTypes.Linked;

public class LinkedResourceType : BaseResourceType
{
    private readonly RdfStoreConnector _rdfStoreConnector;
    private readonly CoscineLDPHelper _coscineLDPHelper;

    public LinkedResourceType(LinkedResourceTypeConfiguration linkedResourceTypeConfiguration) : base(linkedResourceTypeConfiguration)
    {
        if (linkedResourceTypeConfiguration.SparqlEndpoint != null)
        {
            _rdfStoreConnector = new(linkedResourceTypeConfiguration.SparqlEndpoint);
        }
        else
        {
            _rdfStoreConnector = new();
        }
        _coscineLDPHelper = new CoscineLDPHelper(_rdfStoreConnector);
    }

    public override Task CreateResource(string id, long? quota = null, Dictionary<string, string>? options = null)
    {
        // Nothing to do here
        return Task.CompletedTask;
    }

    public override Task DeleteEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        _rdfStoreConnector.SetInvalidation(id, key, "data");

        return Task.CompletedTask;
    }

    public override Task<ResourceEntry?> GetEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        if (!key.StartsWith("/"))
        {
            key = "/" + key;
        }

        var urlId = _coscineLDPHelper.GetId(id, key, true, false, "data");

        var graph = _rdfStoreConnector.GetGraph(urlId);
        if (graph.IsEmpty)
        {
            return Task.FromResult<ResourceEntry?>(null);
        }

        var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(Uris.CoscineLinkedBody));

        if (!triples.Any())
        {
            return Task.FromResult<ResourceEntry?>(null);
        }

        return Task.FromResult<ResourceEntry?>(new ResourceEntry(HttpUtility.UrlDecode(key), true, Encoding.UTF8.GetBytes(triples.First().Object.ToString()).Length, null, null, null, null)); 
    }

    public override Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string>? options = null)
    {
        if (!prefix.StartsWith("/"))
        {
            prefix = "/" + prefix;
        }

        var resultList = new List<ResourceEntry>();

        // Add the results
        var currentDataList = _rdfStoreConnector.ListData(id);
        if (currentDataList is not null)
        {
            var resourceGraphUri = $"https://purl.org/coscine/resources/{id}";
            var currentDataPaths = currentDataList.Select((entry) => entry?[..entry.LastIndexOf("/")].Replace(resourceGraphUri, ""));

            resultList.AddRange(currentDataPaths
                .Where(x => x is not null)
                .Select((x) => GetEntry(id, x!).Result)
                .Where(x => x is not null).Cast<ResourceEntry>());
        }

        return Task.FromResult(resultList);
    }

    public override Task<Stream?> LoadEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        if (!key.StartsWith("/"))
        {
            key = "/" + key;
        }

        var urlId = _coscineLDPHelper.GetId(id, key, true, false, "data");

        var graph = _rdfStoreConnector.GetGraph(urlId);

        if (graph.IsEmpty)
        {
            return Task.FromResult<Stream?>(null);
        }

        var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(Uris.CoscineLinkedBody));

        if (!triples.Any())
        {
            return Task.FromResult<Stream?>(null);
        }

        byte[] byteArray = Encoding.UTF8.GetBytes(triples.First().Object.ToString());
        var stream = new MemoryStream(byteArray);

        return Task.FromResult<Stream?>(stream);
    }

    public override Task StoreEntry(string id, string key, Stream body, Dictionary<string, string>? options = null)
    {
        if (!key.StartsWith("/"))
        {
            key = "/" + key;
        }

        // 4kb + 1
        byte[] buffer = new byte[(1024 * 4) + 1];
        var size = body.Read(buffer, 0, buffer.Length);

        if (size > buffer.Length - 1)
        {
            throw new Exception("Cannot save stream. The stream has more than 4 KB of data.");
        }

        Array.Resize(ref buffer, size);

        var urlId = _coscineLDPHelper.GetId(id, key, true, false, "data");

        var graph = _rdfStoreConnector.GetGraph(urlId);

        if (!graph.IsEmpty)
        {
            var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(Uris.CoscineLinkedBody));
            graph.Retract(triples.ToArray());
        }
        else
        {
            _rdfStoreConnector.CreateNamedGraph(urlId);
        }

        graph.Assert(new Triple(graph.CreateUriNode(urlId), graph.CreateUriNode(Uris.CoscineLinkedBody), graph.CreateLiteralNode(Encoding.UTF8.GetString(buffer))));

        _rdfStoreConnector.AddGraph(graph);

        return Task.CompletedTask;
    }

    public override async Task<ResourceTypeInformation> GetResourceTypeInformation()
    {
        var json = EmbeddedResourceLoader.ReadResource<LinkedResourceType>("LinkedResourceTypeInformation.json");
        var resourceTypeInformation = JsonConvert.DeserializeObject<ResourceTypeInformation>(json)
                                  ?? throw new Exception("LinkedResourceTypeInformation.json could not be parsed.");

        resourceTypeInformation.Status = ResourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden;
        resourceTypeInformation.DisplayName = ResourceTypeConfiguration?.SpecificType?.SpecificTypeName;
        resourceTypeInformation.GeneralType = ResourceTypeConfiguration?.SpecificType?.Type;
        resourceTypeInformation.SpecificType = ResourceTypeConfiguration?.SpecificType?.SpecificTypeName;

        return await Task.FromResult(resourceTypeInformation);
    }

    public override Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task AddPrefix(string id, string prefix, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task UpdateResource(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException(); 
    }

    public override Task<bool> IsResourceCreated(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task DeleteResource(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException(); ;
    }

    public override Task SetResourceQuota(string id, int quota, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task SetResourceReadonly(string id, bool status, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task<Uri> GetPresignedUrl(string id, string key, CoscineHttpVerb httpVerb, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }
}