﻿using Coscine.ResourceTypes.Base;
using Coscine.ResourceTypes.Base.Helpers;
using Coscine.ResourceTypes.Base.Models;
using GitLabApiClient;
using GitLabApiClient.Models.Commits.Requests.CreateCommitRequest;
using GitLabApiClient.Models.Projects.Responses;
using Newtonsoft.Json;
using System.Text;

namespace Coscine.ResourceTypes.GitLab;

public class GitLabResourceType : BaseResourceType
{
    private GitLabClient GitLabClient { get; set; } = null!;
    private GitLabResourceTypeConfiguration _gitLabResourceTypeConfiguration;
    private Project? _gitLabProject;

    public GitLabResourceType(GitLabResourceTypeConfiguration gitLabResourceTypeConfiguration) : base(gitLabResourceTypeConfiguration)
    {
        if (gitLabResourceTypeConfiguration is null)
        {
            throw new ArgumentNullException(nameof(gitLabResourceTypeConfiguration));
        }
        _gitLabResourceTypeConfiguration = gitLabResourceTypeConfiguration;

        // For pre-configured case
        if (_gitLabResourceTypeConfiguration.RepoUrl is not null && _gitLabResourceTypeConfiguration.AccessToken is not null && _gitLabResourceTypeConfiguration.ProjectId > 0)
        {
            GitLabClient = new GitLabClient(new Uri(_gitLabResourceTypeConfiguration.RepoUrl).ToString(), gitLabResourceTypeConfiguration.AccessToken);
            _gitLabProject = RetrieveGitLabProjectAsync(gitLabResourceTypeConfiguration.ProjectId).Result;
        }
    }

    public override Task AddPrefix(string id, string prefix, Dictionary<string, string>? options = null)
    {
        // n.a.
        throw new NotImplementedException();
    }

    public override Task CreateResource(string id, long? quota = null, Dictionary<string, string>? options = null)
    {
        // Equivalent to creating a branch in GitLab, which is not what is intended. Only adding already existing GitLab projects.
        return Task.CompletedTask;
    }

    /// <summary>
    /// Deletes a file from a GitLab project on a specified branch
    /// </summary>
    /// <param name="id">Resource id is ignored</param>
    /// <param name="key">Path containing the file name with extension</param>
    /// <param name="options">Options containing additional information (e.g. commit message)</param>
    /// <returns></returns>
    public override async Task DeleteEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        // Equivalent to deleting a file from Gitlab
        EnsureGitlabConnection();

        if (string.IsNullOrEmpty(_gitLabResourceTypeConfiguration.Branch))
        {
            throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.Branch)}' cannot be null or empty.", nameof(_gitLabResourceTypeConfiguration.Branch));
        }
        // key represents the file name
        if (string.IsNullOrEmpty(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or empty.", nameof(key));
        }

        options = new Dictionary<string, string>()
        {
            { "commit_message", "Coscine Delete" } // Consider adding the user's display name at some point?
        };

        string commitMessage = options["commit_message"];
        if (string.IsNullOrWhiteSpace(commitMessage))
        {
            throw new ArgumentException($"{nameof(options)}[\"commit_message\"] cannot be null, empty or whitespace.");
        }

        // Equivalent to uploading a file in GitLab (committing)
        var actions = new List<CreateCommitRequestAction>
        {
            new CreateCommitRequestAction(CreateCommitRequestActionType.Delete, key),
        };
        await GitLabClient.Commits.CreateAsync(_gitLabProject, new CreateCommitRequest(_gitLabResourceTypeConfiguration.Branch, commitMessage, actions));
    }

    public override Task DeleteResource(string id, Dictionary<string, string>? options = null)
    {
        // Equivalent to deleting the GitLab branch/project
        throw new NotImplementedException();
    }

    /// <summary>
    /// Lists the contents of a single GitLab file on a specified branch
    /// </summary>
    /// <param name="id">Resource id is ignored</param>
    /// <param name="key">Path containing the file name with extension</param>
    /// <param name="options">Additional options (unused)</param>
    public override async Task<ResourceEntry?> GetEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        // Equivalent to loading a file from GitLab
        EnsureGitlabConnection();
        if (string.IsNullOrWhiteSpace(_gitLabResourceTypeConfiguration.Branch))
        {
            throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.Branch)}' cannot be null or whitespace.", nameof(_gitLabResourceTypeConfiguration.Branch));
        }

        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));
        }

        var file = await GitLabClient.Files.GetAsync(_gitLabProject, key, _gitLabResourceTypeConfiguration.Branch);
        var commit = await GitLabClient.Commits.GetAsync(_gitLabProject, file.LastCommitId);
        return new ResourceEntry(key, true, file.Size, null, null, commit.AuthoredDate, commit.CreatedAt);
    }

    public override Task<Uri> GetPresignedUrl(string id, string key, CoscineHttpVerb httpVerb, Dictionary<string, string>? options = null)
    {
        // n.a.
        throw new NotImplementedException();
    }

    public override Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string>? options = null)
    {
        // n.a.
        throw new NotImplementedException();
    }

    public override Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string>? options = null)
    {
        // n.a.
        throw new NotImplementedException();
    }

    public override async Task<ResourceTypeInformation> GetResourceTypeInformation()
    {
        var json = EmbeddedResourceLoader.ReadResource<GitLabResourceType>("GitLabResourceTypeInformation.json");
        var resourceTypeInformation = JsonConvert.DeserializeObject<ResourceTypeInformation>(json)
                                  ?? throw new Exception("GitLabResourceTypeInformation.json could not be parsed.");

        resourceTypeInformation.Status = ResourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden;
        resourceTypeInformation.DisplayName = ResourceTypeConfiguration?.SpecificType?.SpecificTypeName;
        resourceTypeInformation.GeneralType = ResourceTypeConfiguration?.SpecificType?.Type;
        resourceTypeInformation.SpecificType = ResourceTypeConfiguration?.SpecificType?.SpecificTypeName;

        return await Task.FromResult(resourceTypeInformation);
    }

    public override Task<bool> IsResourceCreated(string id, Dictionary<string, string>? options = null)
    {
        // n.a.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Lists the contents of a GitLab project on a specified branch
    /// </summary>
    /// <param name="id">Resource id is ignored</param>
    /// <param name="prefix">File path</param>
    /// <param name="options">Additional options (unused)</param>
    public override async Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string>? options = null)
    {
        // Equivalent to showing folder contents
        EnsureGitlabConnection();

        var tree = await GitLabClient.Trees.GetAsync(_gitLabProject, options =>
        {
            options.Reference = _gitLabResourceTypeConfiguration.Branch;
            options.Path = prefix;
        });

        var entries = new List<ResourceEntry>();

        foreach (var entry in tree)
        {
            if (entry.Type.Equals("blob"))
            {
                var client = new HttpClient();

                if (string.IsNullOrWhiteSpace(_gitLabResourceTypeConfiguration.RepoUrl))
                {
                    throw new ArgumentException("RepoUrl must not be null or white space.", nameof(_gitLabResourceTypeConfiguration.RepoUrl));
                }
                // secureHostUrl is a new string with the scheme set to "https"
                var secureHostUrl = _gitLabResourceTypeConfiguration.RepoUrl.ToLower().Trim().Replace("http://", "https://").TrimEnd('/');

                client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", _gitLabResourceTypeConfiguration.AccessToken);
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Head,
                    RequestUri = new Uri($"{secureHostUrl}/api/v4/projects/{_gitLabResourceTypeConfiguration.ProjectId}/repository/files/{Uri.EscapeDataString(entry.Path)}?ref={_gitLabResourceTypeConfiguration.Branch}"),
                };
                using var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();

                var sizeInBytes = response.Headers.GetValues("X-Gitlab-Size").FirstOrDefault();
                var lastCommitId = response.Headers.GetValues("X-Gitlab-Last-Commit-Id").FirstOrDefault();
                var commit = await GitLabClient.Commits.GetAsync(_gitLabProject, lastCommitId);

                if (sizeInBytes is not null)
                {
                    entries.Add(new ResourceEntry(entry.Path, true, long.Parse(sizeInBytes), null, null, commit.AuthoredDate, commit.CreatedAt));
                }
                else
                {
                    entries.Add(new ResourceEntry(entry.Path, true, 0, null, null, commit.AuthoredDate, commit.CreatedAt));
                }
            }
            else if (entry.Type.Equals("tree"))
            {
                // Folder
                var absolutePath = entry.Path.Last().Equals("/") ? entry.Path : $"{entry.Path}/";
                entries.Add(new ResourceEntry(absolutePath, false, 0, null, null, null, null));
            }
        }
        return entries;
    }

    /// <summary>
    /// Download a file or load a file's contents
    /// </summary>
    /// <param name="id">Resource id is ignored</param>
    /// <param name="key">Path containing the file name with extension</param>
    /// <param name="options">Additional options (unused)</param>
    public override async Task<Stream?> LoadEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        // Equivalent to downloading a file; loading a file's contents
        EnsureGitlabConnection();
        if (string.IsNullOrWhiteSpace(_gitLabResourceTypeConfiguration.Branch))
        {
            throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.Branch)}' cannot be null or whitespace.", nameof(_gitLabResourceTypeConfiguration.Branch));
        }

        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));
        }

        var file = await GitLabClient.Files.GetAsync(_gitLabProject, key, _gitLabResourceTypeConfiguration.Branch);
        if (file.Encoding.Equals("base64"))
        {
            return new MemoryStream(Convert.FromBase64String(file.Content));
        }
        else
        {
            return new MemoryStream(Encoding.ASCII.GetBytes(file.ContentDecoded));
        }
    }

    public override Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string>? options = null)
    {
        // Equivalent to renaming a file (committing). For now not relevant.
        throw new NotImplementedException();
    }

    public override Task SetResourceQuota(string id, int quota, Dictionary<string, string>? options = null)
    {
        // n.a.
        throw new NotImplementedException();
    }

    public override Task SetResourceReadonly(string id, bool status, Dictionary<string, string>? options = null)
    {
        // Equivalent to archiving a project (see GitLab documentation). For now not relevant.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Uploads a file to a GitLab project on a specified branch
    /// </summary>
    /// <param name="id">Resource id is ignored</param>
    /// <param name="key">Path containing the file name with extension</param>
    /// <param name="body">File contents</param>
    /// <param name="options">Additional options containing the commit message</param>
    public override async Task StoreEntry(string id, string key, Stream body, Dictionary<string, string>? options = null)
    {
        EnsureGitlabConnection();
        if (string.IsNullOrEmpty(_gitLabResourceTypeConfiguration.Branch))
        {
            throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.Branch)}' cannot be null or empty.", nameof(_gitLabResourceTypeConfiguration.Branch));
        }
        // key represents the file name
        if (string.IsNullOrEmpty(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or empty.", nameof(key));
        }

        // body represents the file contents
        if (body is null)
        {
            throw new ArgumentNullException(nameof(body));
        }

        options = new Dictionary<string, string>()
        {
            { "commit_message", "Coscine Upload" } // Consider adding the user's display name at some point?
        };

        string commitMessage = options["commit_message"];
        if (string.IsNullOrWhiteSpace(commitMessage))
        {
            throw new ArgumentException($"{nameof(options)}[\"commit_message\"] cannot be null, empty or whitespace.");
        }

        byte[] bytes;
        using (var ms = new MemoryStream())
        {
            body.CopyTo(ms);
            bytes = ms.ToArray();
        }
        var tree = await GitLabClient.Trees.GetAsync(_gitLabProject, options =>
        {
            options.Recursive = true;
            options.Reference = _gitLabResourceTypeConfiguration.Branch;
        });

        var actionType = CreateCommitRequestActionType.Create;

        if (tree.Any(f => f.Path.Equals(key)))
        {
            actionType = CreateCommitRequestActionType.Update;
        }

        var actions = new List<CreateCommitRequestAction>()
         {
            new CreateCommitRequestAction(actionType, key)
            {
                Content = Convert.ToBase64String(bytes),
                Encoding = CreateCommitRequestActionEncoding.Base64
            }
         };
        await GitLabClient.Commits.CreateAsync(_gitLabProject, new CreateCommitRequest(_gitLabResourceTypeConfiguration.Branch, commitMessage, actions));
    }

    /// <summary>
    /// Update the resource to a new project.
    /// </summary>
    /// <param name="id">New Project ID to use</param>
    /// <param name="options">Additional options (unused)</param>
    public override async Task UpdateResource(string id, Dictionary<string, string>? options = null)
    {
        // Equivalent to switching a branch or project token
        EnsureGitlabConnection();
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (int.TryParse(id, out int projectId))
        {
            _gitLabProject = await RetrieveGitLabProjectAsync(projectId);
        }
        else
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be parsed as an integer.", nameof(id));
        }
    }

    private void EnsureGitlabConnection()
    {
        if (GitLabClient is null || _gitLabProject is null)
        {
            if (string.IsNullOrWhiteSpace(_gitLabResourceTypeConfiguration.RepoUrl))
            {
                throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.RepoUrl)}' cannot be null or whitespace.", nameof(_gitLabResourceTypeConfiguration.RepoUrl));
            }
            if (string.IsNullOrWhiteSpace(_gitLabResourceTypeConfiguration.AccessToken))
            {
                throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.AccessToken)}' cannot be null or whitespace.", nameof(_gitLabResourceTypeConfiguration.AccessToken));
            }
            if (_gitLabResourceTypeConfiguration.ProjectId <= 0)
            {
                throw new ArgumentException($"'{nameof(_gitLabResourceTypeConfiguration.ProjectId)}' cannot be less or equal to 0.", nameof(_gitLabResourceTypeConfiguration.ProjectId));
            }
            GitLabClient = new GitLabClient(_gitLabResourceTypeConfiguration.RepoUrl, _gitLabResourceTypeConfiguration.AccessToken);
            _gitLabProject = RetrieveGitLabProjectAsync(_gitLabResourceTypeConfiguration.ProjectId).Result;
        }
    }

    private async Task<Project?> RetrieveGitLabProjectAsync(int projectId)
    {
        // Wrap in a try catch, otherwise Exceptions will be thrown inside Quota methods on expired token
        try
        {
            return await GitLabClient.Projects.GetAsync(projectId);
        }
        catch
        {
            return null;
        }
    }
}