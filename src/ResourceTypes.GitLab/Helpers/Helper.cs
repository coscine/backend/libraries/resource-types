﻿using GitLabApiClient;
using GitLabApiClient.Models.Branches.Responses;
using GitLabApiClient.Models.Projects.Responses;

namespace Coscine.ResourceTypes.GitLab.Helpers;

public class Helper
{
    public static async Task<IEnumerable<Project>> GetAllProjects(Uri domain, string accessToken)
    {
        var gitlabClient = new GitLabClient(domain.ToString(), accessToken);
        var projects = await gitlabClient.Projects.GetAsync(o =>
        {
            o.IsMemberOf = true;
        });
        return projects;
    }

    public static async Task<Project?> GetProject(Uri domain, string accessToken, int projectId)
    {
        var gitlabClient = new GitLabClient(domain.ToString(), accessToken);
        var projects = await gitlabClient.Projects.GetAsync(o =>
        {
            o.IsMemberOf = true;
        });
        var project = projects.FirstOrDefault(p => p.Id.Equals(projectId));
        return project;
    }

    public static async Task<IEnumerable<Branch>> GetBranchesForProject(Uri domain, string accessToken, int projectId)
    {
        var gitlabClient = new GitLabClient(domain.ToString(), accessToken);
        var branches = await gitlabClient.Branches.GetAsync(projectId, o => {});
        return branches;
    }
}
