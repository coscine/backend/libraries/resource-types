using Amazon.S3;
using Amazon.S3.Model;
using Coscine.ECSManager;
using Coscine.ResourceTypes.Base;
using Coscine.ResourceTypes.Base.Helpers;
using Coscine.ResourceTypes.Base.Models;
using Coscine.ResourceTypes.RdsS3.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coscine.ResourceTypes.RdsS3;

public class RdsS3ResourceType : BaseResourceType
{
    private readonly IHttpClientFactory _clientFactory;

    private readonly EcsManager _rdsS3EcsManager;
    private readonly EcsManager _userEcsManager;

    private readonly List<string> _readRights;
    private readonly List<string> _writeRights;

    public RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration => (RdsS3ResourceTypeConfiguration)ResourceTypeConfiguration;

    private readonly AmazonS3Config _amazonS3Config;

    public RdsS3ResourceType(RdsS3ResourceTypeConfiguration rdsS3ResourceTypeOptions) : base(rdsS3ResourceTypeOptions)
    {
        if (rdsS3ResourceTypeOptions is null)
        {
            throw new ArgumentNullException(nameof(rdsS3ResourceTypeOptions));
        }

        if (rdsS3ResourceTypeOptions.Endpoint is null)
        {
            throw new ArgumentException($"{nameof(rdsS3ResourceTypeOptions)}.{nameof(rdsS3ResourceTypeOptions.Endpoint)} cannot be null.");
        }

        _amazonS3Config = new AmazonS3Config
        {
            ServiceURL = RdsS3ResourceTypeConfiguration.Endpoint,
            ForcePathStyle = true
        };

        if (rdsS3ResourceTypeOptions.RdsS3EcsManagerConfiguration is null)
        {
            throw new ArgumentException($"{nameof(rdsS3ResourceTypeOptions)}.{nameof(rdsS3ResourceTypeOptions.RdsS3EcsManagerConfiguration)} cannot be null.");
        }

        _rdsS3EcsManager = new EcsManager
        {
            EcsManagerConfiguration = RdsS3ResourceTypeConfiguration.RdsS3EcsManagerConfiguration
        };

        if (rdsS3ResourceTypeOptions.UserEcsManagerConfiguration is null)
        {
            throw new ArgumentException($"{nameof(rdsS3ResourceTypeOptions)}.{nameof(rdsS3ResourceTypeOptions.UserEcsManagerConfiguration)} cannot be null.");
        }

        _userEcsManager = new EcsManager
        {
            EcsManagerConfiguration = RdsS3ResourceTypeConfiguration.UserEcsManagerConfiguration
        };

        if (rdsS3ResourceTypeOptions.HttpClientFactory is null)
        {
            throw new ArgumentException($"{nameof(rdsS3ResourceTypeOptions)}.{nameof(rdsS3ResourceTypeOptions.HttpClientFactory)} cannot be null.");
        }

        _clientFactory = rdsS3ResourceTypeOptions.HttpClientFactory;

        _readRights = new List<string> { "read", "read_acl" };
        _writeRights = new List<string> { "read", "read_acl", "write", "execute", "privileged_write", "delete" };
    }

    public override async Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (prefix is null)
        {
            throw new ArgumentNullException(nameof(prefix));
        }

        // List all objects
        var listRequest = new ListObjectsRequest
        {
            BucketName = id,
            Prefix = prefix,
            Delimiter = "/"
        };

        using var s3Client = new AmazonS3Client(RdsS3ResourceTypeConfiguration.AccessKey, RdsS3ResourceTypeConfiguration.SecretKey, _amazonS3Config);

        var entries = new List<ResourceEntry>();

        ListObjectsResponse listResponse;
        do
        {
            // Get a list of objects
            listResponse = await s3Client.ListObjectsAsync(listRequest);

            foreach (var commonPrefix in listResponse.CommonPrefixes)
            {
                entries.Add(new ResourceEntry(commonPrefix, false, 0, null, null, DateTime.Now, DateTime.Now));
            }

            foreach (var obj in listResponse.S3Objects)
            {
                // Ensure not adding the same folder, as the prefix to the response
                // Happens, when an empty folder is added through and empty object with an delimiter as the last char of the key.
                if (obj.Key != prefix)
                {
                    entries.Add(new ResourceEntry(obj.Key, obj.Size > 0, obj.Size, null, null, obj.LastModified, obj.LastModified));
                }
            }

            // Set the marker property
            listRequest.Marker = listResponse.NextMarker;
        } while (listResponse.IsTruncated);

        return entries;
    }

    public override async Task<ResourceEntry?> GetEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));
        }

        var url = await GetPresignedUrl(id, key, CoscineHttpVerb.HEAD);
        var responseMessage = await _clientFactory.CreateClient().SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
        var contentLength = responseMessage.Content.Headers.ContentLength;

        if (!contentLength.HasValue)
        {
            throw new Exception("Content-Length could not be extracted.");
        }

        return new ResourceEntry(key, contentLength > 0, contentLength.Value, null, null, DateTime.Now, DateTime.Now);
    }

    public override async Task StoreEntry(string id, string key, Stream body, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (string.IsNullOrEmpty(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or empty.", nameof(key));
        }

        if (body is null)
        {
            throw new ArgumentNullException(nameof(body));
        }

        var url = await GetPresignedUrl(id, key, CoscineHttpVerb.PUT);
        var response = await _clientFactory.CreateClient().PutAsync(url, new StreamContent(body));

        if (!response.IsSuccessStatusCode)
        {
            throw new AggregateException();
        }
    }

    public override async Task DeleteEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));
        }

        var url = await GetPresignedUrl(id, key, CoscineHttpVerb.DELETE);
        var responseMessage = await _clientFactory.CreateClient().DeleteAsync(url);
        responseMessage.Content.ReadAsStream();
    }

    public override async Task<Stream?> LoadEntry(string id, string key, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));
        }

        var url = await GetPresignedUrl(id, key, CoscineHttpVerb.GET);
        var responseMessage = await _clientFactory.CreateClient().GetAsync(url, HttpCompletionOption.ResponseHeadersRead);
        return await responseMessage.Content.ReadAsStreamAsync();
    }

    public override async Task SetResourceReadonly(string id, bool status, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (string.IsNullOrWhiteSpace(RdsS3ResourceTypeConfiguration.AccessKey))
        {
            throw new ArgumentException($"{nameof(RdsS3ResourceTypeConfiguration)}.{nameof(RdsS3ResourceTypeConfiguration.AccessKey)} cannot be null.");
        }

        if (string.IsNullOrWhiteSpace(RdsS3ResourceTypeConfiguration.AccessKeyRead))
        {
            throw new ArgumentException($"{nameof(RdsS3ResourceTypeConfiguration)}.{nameof(RdsS3ResourceTypeConfiguration.AccessKeyRead)} cannot be null.");
        }

        if (string.IsNullOrWhiteSpace(RdsS3ResourceTypeConfiguration.AccessKeyWrite))
        {
            throw new ArgumentException($"{nameof(RdsS3ResourceTypeConfiguration)}.{nameof(RdsS3ResourceTypeConfiguration.AccessKeyWrite)} cannot be null.");
        }

        var policy = GenerateAccessPolicy(RdsS3ResourceTypeConfiguration.AccessKey, RdsS3ResourceTypeConfiguration.AccessKeyWrite, RdsS3ResourceTypeConfiguration.AccessKeyRead, id, status);

        var putRequest = new PutBucketPolicyRequest
        {
            BucketName = id,
            Policy = policy
        };

        using var _s3client = new AmazonS3Client(RdsS3ResourceTypeConfiguration.AccessKey, RdsS3ResourceTypeConfiguration.SecretKey, _amazonS3Config);
        try
        {
            await _s3client.PutBucketPolicyAsync(putRequest);
        }
#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
        catch (Exception)
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.
        {
        }
    }

    public override async Task CreateResource(string id, long? quota = null, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (quota is null)
        {
            throw new ArgumentNullException(nameof(quota));
        }

        await _rdsS3EcsManager.CreateBucket(id, quota.Value);
        await _userEcsManager.CreateObjectUser(RdsS3ResourceTypeConfiguration.AccessKeyRead, RdsS3ResourceTypeConfiguration.SecretKeyRead);
        await _userEcsManager.CreateObjectUser(RdsS3ResourceTypeConfiguration.AccessKeyWrite, RdsS3ResourceTypeConfiguration.SecretKeyWrite);

        await _rdsS3EcsManager.SetUserAcl(RdsS3ResourceTypeConfiguration.AccessKeyRead, id, _readRights);
        await _rdsS3EcsManager.SetUserAcl(RdsS3ResourceTypeConfiguration.AccessKeyWrite, id, _writeRights);

        // Set to the read only value, if present.
        if (options?.ContainsKey("readonly") == true && bool.TryParse(options["readonly"], out var result) && result)
        {
            await SetResourceReadonly(id, result, options);
        }
        else
        {
            await SetResourceReadonly(id, false, options);
        }
    }

    public override async Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        return await _rdsS3EcsManager.GetBucketTotalUsedQuota(id);
    }

    public override async Task<long> GetResourceQuotaAvailable(string id, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        return await _rdsS3EcsManager.GetBucketQuota(id);
    }

    public override async Task SetResourceQuota(string id, int quota, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        await _rdsS3EcsManager.SetBucketQuota(id, quota);
    }

    public override async Task<Uri> GetPresignedUrl(string id, string key, CoscineHttpVerb httpVerb, Dictionary<string, string>? options = null)
    {
        if (string.IsNullOrWhiteSpace(id))
        {
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        }

        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"'{nameof(key)}' cannot be null or whitespace.", nameof(key));
        }

        if (string.IsNullOrWhiteSpace(RdsS3ResourceTypeConfiguration.Endpoint))
        {
            throw new ArgumentException($"{nameof(RdsS3ResourceTypeConfiguration)}.{nameof(RdsS3ResourceTypeConfiguration.Endpoint)} cannot be null.");
        }

        return await Task.Run(() =>
        {
            var amazonConfig = new AmazonS3Config
            {
                ServiceURL = RdsS3ResourceTypeConfiguration.Endpoint,
                ForcePathStyle = true
            };

            using var s3Client = new AmazonS3Client(RdsS3ResourceTypeConfiguration.AccessKey, RdsS3ResourceTypeConfiguration.SecretKey, _amazonS3Config);
            var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = id,
                Key = key,
                Verb = Helper.GetVerb(httpVerb),
                // The Simulator uses HTTP and the live system HTTPS
                Protocol = RdsS3ResourceTypeConfiguration.Endpoint.Contains("ecs-sim01.itc.rwth-aachen.de") ? Protocol.HTTP : Protocol.HTTPS,
                // For now, expiry of a day is set, but this might be up to debate
                Expires = DateTime.UtcNow.AddHours(24)
            });
            return new Uri(presignedUrl);
        });
    }

    public override async Task<ResourceTypeInformation> GetResourceTypeInformation()
    {
        var json = EmbeddedResourceLoader.ReadResource<RdsS3ResourceType>("RdsS3ResourceTypeInformation.json");
        var resourceTypeInformation = JsonConvert.DeserializeObject<ResourceTypeInformation>(json)
                                      ?? throw new Exception("RdsS3ResourceTypeInformation.json could not be parsed.");

        resourceTypeInformation.Status = ResourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden;
        resourceTypeInformation.DisplayName = ResourceTypeConfiguration?.SpecificType?.SpecificTypeName;
        resourceTypeInformation.GeneralType = ResourceTypeConfiguration?.SpecificType?.Type;
        resourceTypeInformation.SpecificType = ResourceTypeConfiguration?.SpecificType?.SpecificTypeName;

        return await Task.FromResult(resourceTypeInformation);
    }

    private static string GenerateAccessPolicy(string accessKey, string writeKey, string accessKeyRead, string bucketname, bool isReadonly)
    {
        if (isReadonly)
        {
            var json = EmbeddedResourceLoader.ReadResource<RdsS3ResourceType>("BucketReadPolicy.json");
            var jObject = JsonConvert.DeserializeObject<JObject>(json)
                                  ?? throw new Exception("BucketReadPolicy.json could not be parsed.");
            {
                var allowStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "AllowStatement") as JObject;
                (allowStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (allowStatement?["Principal"] as JArray)?.Add($"{writeKey}");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKey}");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKeyRead}");
            }
            {
                var denyStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "DenyStatement") as JObject;
                (denyStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (denyStatement?["Principal"] as JArray)?.Add($"{writeKey}");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKey}");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKeyRead}");
            }

            return jObject.ToString();
        }
        else
        {
            var json = EmbeddedResourceLoader.ReadResource<RdsS3ResourceType>("BucketWritePolicy.json");
            var jObject = JsonConvert.DeserializeObject<JObject>(json)
                                  ?? throw new Exception("BucketWritePolicy.json could not be parsed.");
            {
                var allowStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "WriteStatement") as JObject;
                (allowStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (allowStatement?["Principal"] as JArray)?.Add($"{writeKey}");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKey}");
            }
            {
                var denyStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "ReadStatement") as JObject;
                (denyStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKeyRead}");
            }

            return jObject.ToString();
        }
    }

    public override Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task AddPrefix(string id, string prefix, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task UpdateResource(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task<bool> IsResourceCreated(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }

    public override Task DeleteResource(string id, Dictionary<string, string>? options = null)
    {
        throw new NotImplementedException();
    }
}